/*
 * calc_point_mass.h
 *
 *  Created on: Mar 4, 2013
 *      Author: andrew.somerville
 */

#ifndef CALC_POINT_MASS_H_
#define CALC_POINT_MASS_H_


#include <re2/kdltools/kdl_tree_util.hpp>

#include <kdl/tree.hpp>

#include <Eigen/Geometry>

#include <vector>
#include <set>


Eigen::Vector4d calcPointMassOld( const KDL::Tree &        tree,
                                  const KDL::TreeElement & element,
                                  const Eigen::VectorXd &  jointAngles );

Eigen::Vector4d calcPointMass(  const KDL::Tree &        tree,
                                const KDL::TreeElement & element,
                                const Eigen::VectorXd &  jointAngles );

Eigen::Vector4d calcPointMass0( const KDL::Tree &        tree,
                                const KDL::TreeElement & element,
                                const Eigen::VectorXd &  jointAngles );

Eigen::Vector4d calcPointMass1( const KDL::Tree &        tree,
                                const KDL::TreeElement & element,
                                const Eigen::VectorXd &  jointAngles );

void calcPointMass_internal( const KDL::Tree &        tree,
                             const Eigen::Affine3d &  toLowerFrame,
                             const Eigen::Affine3d &  toBaseFrame,
                             const Eigen::Vector3d &  jointLocation,
                             const KdlElementChildren & children,
                             const KDL::TreeElement & element,
                             const Eigen::VectorXd &  jointAngles,
                             const Eigen::Vector4d &  localPointMass,
                             Eigen::Vector4d &        postPointMass_out,
                             const int                jointIndex,
                             std::set<const KDL::TreeElement*> &      visited_out );

void calcPointMassBackward( const KDL::Tree &        tree,
                            const Eigen::Affine3d &  lastToBaseFrame,
                            const KDL::TreeElement & prevElement,
                            const Eigen::VectorXd &  jointAngles,
                            Eigen::Vector4d &        postPointMass_out,
                            std::set<const KDL::TreeElement*> &      visited_out );


void calcPointMassForward( const KDL::Tree &        tree,
                           const Eigen::Affine3d &  lastToBaseFrame,
                           const KDL::TreeElement & element,
                           const Eigen::VectorXd &  jointAngles,
                           Eigen::Vector4d &        postPointMass_out,
                           std::set<const KDL::TreeElement*> &      visited_out );

Eigen::Vector4d recursivePointMass( const KDL::TreeElement & element,
                                    const KDL::Tree & tree,
                                    const Eigen::VectorXd & jointAngles,
                                    const Eigen::Affine3d & toParent,
                                    std::vector<bool> & visited );


#endif /* CALC_POINT_MASS_H_ */
