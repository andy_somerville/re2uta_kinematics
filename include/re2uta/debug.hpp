/*
 * debug.hpp
 *
 *  Created on: Mar 21, 2013
 *      Author: andrew.somerville
 */

#ifndef DEBUG_HPP_
#define DEBUG_HPP_

#include <re2uta/colors.hpp>

#include <re2/visutils/VisualDebugPublisher.h>
#include <re2uta/AtlasDebugPublisher.hpp>

#include <boost/functional/hash.hpp>


extern boost::shared_ptr<re2::VisualDebugPublisher>   vizDebugPublisher;
extern boost::shared_ptr<re2uta::AtlasDebugPublisher> atlasDebugPublisher;

extern bool debugSingleStep;

typedef boost::hash<std::string>  BoostStringHash;

Eigen::Vector4d colorFromHash( int hash );


#endif /* DEBUG_HPP_ */
