/*
 * kinematics.hpp
 *
 *  Created on: Nov 21, 2012
 *      Author: somervil
 */

#ifndef KINEMATICS_HPP_
#define KINEMATICS_HPP_



#include <kdl/chainjnttojacsolver.hpp>
#include <kdl/segment.hpp>
#include <kdl/joint.hpp>
#include <kdl/jacobian.hpp>

#include <Eigen/Core>
#include <Eigen/Geometry>

#include <boost/shared_ptr.hpp>

namespace re2uta
{

//typedef boost::shared_ptr<KDL::Jacobian> JacobianPtr;

//JacobianPtr calculateJacobian( const KDL::Chain & chain, const KDL::JntArray & currentJointPositions, const std::vector<bool> & lockedJoints, const int numUnlockedJoints );
KDL::Jacobian calculateJacobian( const KDL::Chain & chain, const KDL::JntArray & currentJointPositions, const std::vector<bool> & lockedJoints, const int numUnlockedJoints );
//JacobianPtr calculateJacobianFromCondensedChain( const KDL::Chain & condensedChain, const KDL::JntArray& currentJointPositions );
KDL::Jacobian calculateJacobianFromCondensedChain( const KDL::Chain & condensedChain, const KDL::JntArray& currentJointPositions );

//TwistVectorPtr calculateJacobianFromCondensedChain( const Chain & condensedChain, const JntArray& currentJointPositions )

//
//
//TwistVectorPtr calculateTwistTree( const Tree & tree )


}




#endif /* KINEMATICS_HPP_ */
