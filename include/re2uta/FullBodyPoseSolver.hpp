/*
 * FullBodyPoseSolver.hpp
 *
 *  Created on: Mar 22, 2013
 *      Author: andrew.somerville
 */

#ifndef FULLBODYPOSESOLVER_HPP_
#define FULLBODYPOSESOLVER_HPP_

#include <re2uta/constraints/SolverConstraint.hpp>
#include <re2uta/constraints/CenterOfMassConstraint.hpp>
#include <re2uta/constraints/FrameConstraint.hpp>
#include <re2uta/constraints/InputConstraint.hpp>
#include <re2uta/pinv.hpp>
#include <re2/eigen/eigen_util.h>
#include <re2/kdltools/kdl_tree_util.hpp>
#include <kdl/tree.hpp>

#include <Eigen/Core>
#include <Eigen/Geometry>

#include <boost/shared_ptr.hpp>

#include <string>

namespace re2uta
{


class FullBodyPoseSolver
{
    public:
        typedef boost::shared_ptr<FullBodyPoseSolver> Ptr;

    public:
        FullBodyPoseSolver( const KDL::Tree & tree,
                            const Eigen::VectorXd & minInputValues = Eigen::VectorXd(),
                            const Eigen::VectorXd & maxInputValues = Eigen::VectorXd(),
                            const std::string & baseSegmentName    = std::string( "l_foot" ),
                            const std::string &oppFootSegmentName  = std::string( "r_foot" ),
                            const std::string & postureSegmentName = std::string( "utorso" )  );

        FullBodyPoseSolver( const urdf::Model & model,
                            const std::string & baseSegmentName    = std::string( "l_foot" ),
                            const std::string & oppFootSegmentName = std::string( "r_foot" ),
                            const std::string & postureSegmentName = std::string( "utorso" )  );

        void
        init( const KdlTreeConstPtr & tree,
              const Eigen::VectorXd & minInputValues,
              const Eigen::VectorXd & maxInputValues,
              const std::string & baseSegmentName     = std::string( "l_foot" ),
              const std::string & oppFootSegmentName  = std::string( "r_foot" ),
              const std::string & postureSegmentName  = std::string( "utorso" )  );



    public:
        // warning, frames added this way must have their bases changed manually
        FrameConstraint::Ptr addFrameConstraint( const std::string &      baseElementName, const std::string &     tipElementName );
        FrameConstraint::Ptr addFrameConstraint( const KDL::TreeElement * baseElement,    const KDL::TreeElement * tipElement     );
        bool                 removeFrameConstraint( const FrameConstraint::Ptr & frameConstraint );


        Eigen::VectorXd
        solvePose(const std::string &     baseElementName,
                  const std::string &     oppositeFoot,
                  const Eigen::VectorXd & jointPositions,
                  const Eigen::Vector6d & desiredOppositeFootPosition,
                  const Eigen::Vector3d & desiredPostureOrientation,
                  const Eigen::Vector3d & desiredComPose,
                  const Eigen::VectorXd & desiredJointPose );


        //FIXME this should be renamed, but it solves the out put vector given a seed input vector
        //dont for get to set constraint coals before starting this thing
        // solution returns empty if failed
        // will return inf and nan, which it will not accept back
        Eigen::VectorXd
        solvePose(const Eigen::VectorXd & jointPositions, int maxIterations = 500 );

        //This funciton probably belongs inside a cartesian position controller instead
        Eigen::VectorXd
        solveVelocity( const Eigen::VectorXd & jointPositions, const Eigen::VectorXd & desiredOutputVelocity );


        void
        setDefaultJointPose( const Eigen::VectorXd & jointPositions );


        Eigen::VectorXd
        calcCurrentPose(const Eigen::VectorXd &  jointPositions,
                        const Eigen::Affine3d &  defaultTransform );

        Eigen::VectorXd
        calcCurrentVelocity( const Eigen::VectorXd &  jointPositions,
                             const Eigen::VectorXd &  jointVelocities,
                             const Eigen::Affine3d &  defaultTransform );





        Eigen::MatrixXd
        calcMasterJacobian( const Eigen::VectorXd &  jointPositions,
                            const Eigen::Affine3d &  defaultTransform );


        CenterOfMassConstraint::Ptr comConstraint()          { return m_comConstraint;         }
        FrameConstraint::Ptr        oppFootConstraint()      { return m_oppFootConstraint;     }
        FrameConstraint::Ptr        postureConstraint()      { return m_postureConstraint;     }
        InputConstraint::Ptr        optimalAngleConstraint() { return m_optimalAngleConstraint;}



        bool
        shouldStopSolving( const Eigen::VectorXd & currentOutputPose, const Eigen::VectorXd & targetOutputPose, int iterations, int maxIterations );

        Eigen::Vector6d
        getPoseTwist( const std::string & baseFrame,
                      const std::string & tipFrame,
                      const Eigen::VectorXd & jointPositions );

        Eigen::Affine3d
        getPose( const std::string & baseFrame,
                 const std::string & tipFrame,
                 const Eigen::VectorXd & jointPositions );

        Eigen::Affine3d
        getTransformFromTo( const std::string & baseFrame,
                            const std::string & tipFrame,
                            const Eigen::VectorXd & jointPositions );

        Eigen::Vector3d
        getCenterOfMass( const std::string & baseFrame,
                         const Eigen::VectorXd & jointPositions );

        KdlTreeConstPtr              tree() { return m_tree; }
        const SolverConstraintList & constraints(){ return m_solverConstraints; }
        int                          goalDims()   { return m_goalDims; }


    private:
        KdlTreeConstPtr             m_tree;
        int                         m_numJoints;
        int                         m_goalDims;
        KdlTreeFkSolverPtr          m_fkSolver;
        Eigen::VectorXd             m_inputMinValues;
        Eigen::VectorXd             m_inputMaxValues;

        SolverConstraintList        m_solverConstraints;

        CenterOfMassConstraint::Ptr m_comConstraint;
        FrameConstraint::Ptr        m_oppFootConstraint;
        FrameConstraint::Ptr        m_postureConstraint;
        InputConstraint::Ptr        m_optimalAngleConstraint;

        const KDL::TreeElement * m_postureElement;

    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW


};


}

#endif /* FULLBODYPOSESOLVER_HPP_ */
