/*
 * SolverConstraint.cpp
 *
 *  Created on: Apr 23, 2013
 *      Author: andrew.somerville
 */

#pragma once


#include <Eigen/Core>
#include <boost/shared_ptr.hpp>
#include <vector>
#include <list>

namespace re2uta
{

class SolverConstraint
{
    public:
        typedef boost::shared_ptr<SolverConstraint> Ptr;

    public:
        SolverConstraint( int startRow, int dims );
        virtual ~SolverConstraint() {};
        virtual Eigen::MatrixXd     generateJacobian( const Eigen::VectorXd & currentInput ) = 0;
        virtual Eigen::VectorXd     getCurrentOutput( const Eigen::VectorXd & currentInput ) = 0;
        virtual Eigen::VectorXd     getPositionError( const Eigen::VectorXd & currentInput );
        virtual Eigen::VectorXd     getVelocityError( const Eigen::VectorXd & currentInput, const Eigen::VectorXd & currentInputDt );
        virtual bool                solutionReached(  const Eigen::VectorXd & currentOutput, int iterations ) = 0;
        virtual int                 dimensions() const = 0;

        virtual int                 startRow();
        virtual void                setStartRow( int row );
        virtual void                internal_setStartRow( int startRow );
        virtual void                setGoal(             const Eigen::VectorXd & goalOutput   );
        virtual Eigen::VectorXd &   goal();
        virtual void                setGoalWeightVector( const Eigen::VectorXd & goalWeights   );
        virtual Eigen::MatrixXd::DiagonalReturnType goalWeightVector();
        virtual void                setGoalWeightMatrix( const Eigen::MatrixXd & goalWeights   );
        virtual Eigen::MatrixXd &   goalWeightMatrix();
        virtual const std::string   name();

        Eigen::VectorXd &           posKp() { return m_posKp;  }
        Eigen::VectorXd &           posKd() { return m_posKd;  }
        Eigen::VectorXd &           velKp() { return m_velKp;  }
        Eigen::VectorXd &           velMax(){ return m_velMax; }

    protected:
        int              m_startRow;
        int              m_dimensions;
        Eigen::VectorXd  m_goalOutput;
        Eigen::MatrixXd  m_goalWeights;
        std::string      m_name;

        Eigen::VectorXd  m_posKp;
        Eigen::VectorXd  m_posKd;
        Eigen::VectorXd  m_velKp;
        Eigen::VectorXd  m_velMax;

    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW

};



typedef std::vector<SolverConstraint::Ptr> SolverConstraintVector;
typedef std::list<SolverConstraint::Ptr>   SolverConstraintList;

}
