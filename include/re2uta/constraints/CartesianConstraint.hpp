/*
 * CartesianConstraint.hpp
 *
 *  Created on: Jun 3, 2013
 *      Author: andrew.somerville
 */

#pragma once

#include <re2uta/constraints/SolverConstraint.hpp>
#include <re2/kdltools/kdl_tree_util.hpp>
#include <re2/eigen/eigen_util.h>
#include <Eigen/Core>
#include <string>

namespace KDL { class TreeElement; }

namespace re2uta
{

class CartesianConstraint : public SolverConstraint
{
    public:
        typedef boost::shared_ptr<CartesianConstraint> Ptr;

    public:
        CartesianConstraint( int startRow,
                             int dims,
                             const KdlTreeConstPtr & tree,
                             const KDL::TreeElement * baseElement,
                             const KdlTreeFkSolverPtr & fkSolver = KdlTreeFkSolverPtr() );

        virtual ~CartesianConstraint();

        virtual const KDL::TreeElement *  baseTreeElement();
        virtual const std::string         baseFrameId()    ;

        virtual const KDL::TreeElement * setBaseElement(      const KDL::TreeElement * baseElement );
        virtual const KDL::TreeElement * setBaseElement(      const std::string &      frameName   );

        virtual const KDL::TreeElement * convertToNewBase( const std::string & baseElement, const Eigen::VectorXd & jointPositions );


    protected:
        virtual void updateName();

        const KDL::TreeElement * m_baseElement;
        KdlTreeConstPtr          m_tree;
        KdlTreeFkSolverPtr       m_fkSolver;

    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};

}
