/*
 * CenterOfMassConstraint.hpp
 *
 *  Created on: Apr 23, 2013
 *      Author: andrew.somerville
 */

#pragma once

#include <re2uta/center_of_mass_jacobian.hpp>
#include <re2uta/calc_point_mass.hpp>
#include <re2uta/constraints/SolverConstraint.hpp>
#include <re2uta/constraints/CartesianConstraint.hpp>
#include <re2/kdltools/kdl_tree_util.hpp>
#include <rosconsole/macros_generated.h>

namespace re2uta
{

class CenterOfMassConstraint : public CartesianConstraint
{
    public:
        typedef boost::shared_ptr<CenterOfMassConstraint> Ptr;

    public:
        CenterOfMassConstraint( int startRow, KdlTreeConstPtr tree, const KDL::TreeElement * baseElement );
        virtual ~CenterOfMassConstraint();

        void                     setDefaultTransform( const Eigen::Affine3d & defaultTransform );
        virtual Eigen::MatrixXd  generateJacobian( const Eigen::VectorXd & currentInput );
        virtual Eigen::VectorXd  getCurrentOutput( const Eigen::VectorXd & currentInput );
        virtual Eigen::VectorXd  getPositionError( const Eigen::VectorXd & currentInput );
        virtual Eigen::VectorXd  getVelocityError( const Eigen::VectorXd & currentInput, const Eigen::VectorXd & currentInputDt );
        virtual bool             solutionReached(  const Eigen::VectorXd & currentOutput, int iterations );

        virtual int              dimensions() const;

    protected:
        virtual void             updateName();

    private:
        double                   m_treeMass;
        Eigen::Affine3d          m_defaultTransform;

    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};


}



