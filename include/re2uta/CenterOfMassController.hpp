/*
 * CenterOfMassController.hpp
 *
 *  Created on: Mar 4, 2013
 *      Author: andrew.somerville
 */

#ifndef CENTEROFMASSCONTROLLER_HPP_
#define CENTEROFMASSCONTROLLER_HPP_


#include <kdl/tree.hpp>
#include <kdl/treefksolverpos_recursive.hpp>

#include <Eigen/Geometry>

#include <ros/ros.h>

#include <boost/shared_ptr.hpp>



namespace Eigen
{
    typedef Eigen::Matrix<double,6,1> Vector6d;
}


namespace re2uta
{

class CenterOfMassController
{
    public:
//        typedef boost::shared_ptr<Eigen::MatrixXd>                MatrixXdPtr;
//        typedef boost::shared_ptr<Eigen::Affine3d>                Affine3dPtr;
        typedef std::map<std::string, double>                     JointPositionMap;
        typedef boost::shared_ptr<KDL::TreeFkSolverPos_recursive> TreeFKSolverPtr;

    public:
        CenterOfMassController( const std::string & urdfString );

        CenterOfMassController( const KDL::Tree & tree );

        void init();


        Eigen::VectorXd
        calcVelocitiesForComCommand( const Eigen::VectorXd & jointPositions,
                                     const Eigen::Vector3d & comDesiredVelocity,
                                     const Eigen::Vector6d & rFootDesiredPoseInLFootFrame );


        void
        commandJointVelocities( const Eigen::VectorXd & jointVelocityVector );


        Eigen::MatrixXd
        calcTipJacobian( const KDL::TreeElement * baseElement,
                         const KDL::TreeElement * tipElement,
                         const Eigen::VectorXd &  jointPositions,
                         const Eigen::Affine3d &  defaultTransform = Eigen::Affine3d::Identity() );


        Eigen::MatrixXd
        calcComJacobian( const KDL::TreeElement * treeElement,
                         const Eigen::VectorXd &  jointPositions,
                         const Eigen::Affine3d &  defaultTransform = Eigen::Affine3d::Identity() );


        Eigen::Affine3d
        getTransform( const std::string & baseFrame, const std::string & tipFrame, const Eigen::VectorXd & jointPositions );


        Eigen::Vector6d
        getPose( const std::string & baseFrame, const std::string & tipFrame, const Eigen::VectorXd & jointPositions );

        Eigen::Vector4d
        calcComPointMass( const Eigen::VectorXd & jointPositions );

        int getJointIndex( const std::string & segmentName );

    private:


        ros::NodeHandle          m_node;
        KDL::Tree                m_tree;
//        KDL::JntArray            m_jointPositions;
//        JointPositionMap         m_jointPositionMap;
        TreeFKSolverPtr          m_treeFkSolver;
        double                   m_totalMass;

        const KDL::TreeElement * m_rootElement;
        const KDL::TreeElement * m_leftFootElement;
        const KDL::TreeElement * m_rightFootElement;
        const KDL::TreeElement * m_rlglutElement;

    public:
        Eigen::MatrixXd          m_lastComJacobianFromLFoot;
        Eigen::MatrixXd          m_lastTipJacobianRFootInLFoot;

};


}



#endif /* CENTEROFMASSCONTROLLER_HPP_ */
