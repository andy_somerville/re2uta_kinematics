/*
 * center_of_mass_jacobian.h
 *
 *  Created on: Feb 22, 2013
 *      Author: andrew.somerville
 */

#ifndef CENTER_OF_MASS_JACOBIAN_H_
#define CENTER_OF_MASS_JACOBIAN_H_

#include <re2/visutils/VisualDebugPublisher.h>
#include <re2/kdltools/kdl_tree_util.hpp>

#include <ros/ros.h>

#include <kdl/segment.hpp>
#include <kdl/tree.hpp>
#include <Eigen/Core>
#include <Eigen/Geometry>



Eigen::MatrixXd
calcTreeComJacobian( const KDL::Tree &        tree,
                     const KDL::TreeElement * treeElement,
                     const Eigen::VectorXd &  jointPositions,
                     const Eigen::Affine3d &  defaultTransform,
                     double                   totalMass );




#endif /* CENTER_OF_MASS_JACOBIAN_H_ */
