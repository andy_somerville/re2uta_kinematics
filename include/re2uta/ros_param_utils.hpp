/*
 * ros_param_utils.hpp
 *
 *  Created on: May 1, 2014
 *      Author: andrew.somerville
 *
 *  Notes: this uses c++11
 *
 *
 *
 *
 *
 *
 */

#pragma once

#include <ros/node_handle.h>
#include <ros/ros.h>

#include <boost/foreach.hpp>

#include <string>

template<typename FuncT, typename CollectionT >
void traverseParameterCollection( ros::NodeHandle node, const std::string & paramKey, FuncT func )
{
    CollectionT valueCollection;

    if( node.getParam(paramKey, valueCollection ) )
    {
        ROS_INFO_STREAM( "found : "     << paramKey );
        BOOST_FOREACH( typename CollectionT::value_type const & entry, valueCollection  )
        {
            func( entry );
        }
    }
    else
    {
        ROS_WARN_STREAM( "cant find : " << paramKey );
    }
}

template< typename ValueT, typename FuncT>
void traverseParameterMap( ros::NodeHandle node, const std::string & paramKey, FuncT func )
{
    typedef std::map<std::string,ValueT> StringToValueTMap;
    traverseParameterCollection<FuncT, StringToValueTMap>( node, paramKey, func );
}

template< typename ValueT, typename FuncT>
void traverseParameterVector( ros::NodeHandle node, const std::string & paramKey, FuncT func )
{
    typedef std::vector<ValueT> ValueTList;
    traverseParameterCollection<FuncT, ValueTList>( node, paramKey, func );
}



