/*
 * center_of_mass_position.hpp
 *
 *  Created on: Mar 15, 2013
 *      Author: andrew.somerville
 */

#ifndef CENTER_OF_MASS_POSITION_HPP_
#define CENTER_OF_MASS_POSITION_HPP_


#include <re2/kdltools/kdl_tree_util.hpp>

#include <kdl/treefksolverpos_recursive.hpp>

#include <Eigen/Core>
#include <Eigen/Geometry>

#include <boost/function.hpp>



typedef boost::function<Eigen::MatrixXd (const Eigen::VectorXd &)>                   JacobianFunctor;
typedef boost::function<Eigen::VectorXd (const Eigen::VectorXd &)>                   FkSolveFunctor;
typedef boost::function<bool (const Eigen::VectorXd&, const Eigen::VectorXd&, int)>  GuardFunctor;

Eigen::MatrixXd
solveGradientDescent( const Eigen::VectorXd & inputStateSeed,
                     JacobianFunctor &       generateJacobian,
                     FkSolveFunctor &        solveFk,
                     const Eigen::VectorXd & desiredOutputState,
                     GuardFunctor &          solutionReached,
                     const Eigen::VectorXd & minInputValues     = Eigen::VectorXd(),
                     const Eigen::VectorXd & maxInputValues     = Eigen::VectorXd(),
                     const Eigen::VectorXd & optimumInputValues = Eigen::VectorXd(),
                     const Eigen::MatrixXd & inputWeightMatrix  = Eigen::MatrixXd(),
                     const Eigen::MatrixXd & outputWeightMatrix = Eigen::MatrixXd() );

Eigen::VectorXd
calcPenalty( const Eigen::VectorXd & mins,
             const Eigen::VectorXd & maxs,
             const Eigen::VectorXd & centers,
             const Eigen::VectorXd & inputValues,
             const double penaltyRangeFactor );

Eigen::VectorXd
calcNormalizedIntrusion( const Eigen::VectorXd & mins,
                         const Eigen::VectorXd & maxs,
                         const Eigen::VectorXd & centers,
                         const Eigen::VectorXd & inputValues,
                         const double penaltyRangeFactor );
#endif /* CENTER_OF_MASS_POSITION_HPP_ */
