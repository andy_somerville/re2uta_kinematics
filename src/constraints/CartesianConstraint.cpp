/*
 * CartesianConstraint.cpp
 *
 *  Created on: Jun 10, 2013
 *      Author: andrew.somerville
 */

#include <re2uta/constraints/CartesianConstraint.hpp>
#include <ros/ros.h>
#include <boost/lexical_cast.hpp>

namespace re2uta
{



CartesianConstraint::
CartesianConstraint( int startRow, int dims, const KdlTreeConstPtr & tree, const KDL::TreeElement * baseElement, const KdlTreeFkSolverPtr & fkSolver )
    : SolverConstraint( startRow, dims )
{
    m_tree = tree;

    setBaseElement( baseElement );

    if( fkSolver )
        m_fkSolver = fkSolver;
    else
        m_fkSolver.reset( new KDL::TreeFkSolverPos_recursive( *tree ) );
}

CartesianConstraint::
~CartesianConstraint()
{}

const KDL::TreeElement *
CartesianConstraint::
baseTreeElement()
{
    return m_baseElement;

}


const std::string
CartesianConstraint::
baseFrameId()
{
    if( m_baseElement != NULL )
        return m_baseElement->segment.getName();
    else
        return std::string();
}


const KDL::TreeElement *
CartesianConstraint::
setBaseElement(      const std::string & frameName )
{

    const KDL::TreeElement * baseElement;
    try
    {
        baseElement = &getTreeElement( *m_tree, frameName );
    }
    catch( std::runtime_error & error )
    {
        ROS_ERROR( "Base base element in Frame Constraint!" );
        baseElement = NULL;
    }

    setBaseElement( baseElement );

    return baseElement;
}


const KDL::TreeElement *
CartesianConstraint::
setBaseElement(      const KDL::TreeElement * baseElement )
{
    m_baseElement = baseElement;

    updateName();

    return m_baseElement;
}


void
CartesianConstraint::
updateName()
{
    if( m_baseElement )
        m_name = m_baseElement->segment.getName() + boost::lexical_cast<std::string>( m_startRow );

}

Eigen::Affine3d
eulerXyzToRot( const Eigen::Vector3d & rpy ) //FIXME convention correct!?!?!?!?!?
{
    Eigen::Affine3d rotation = Eigen::Affine3d::Identity();

    rotation = Eigen::AngleAxisd( rpy(0), Eigen::ZAxis3d ) * rotation; //FIXME unknown if this is correct
    rotation = Eigen::AngleAxisd( rpy(1), Eigen::YAxis3d ) * rotation; //FIXME unknown if this is correct
    rotation = Eigen::AngleAxisd( rpy(2), Eigen::ZAxis3d ) * rotation; //FIXME unknown if this is correct

    return rotation;
}

Eigen::Vector6d
transformTwist( const Eigen::Affine3d & transform, const Eigen::Vector6d & twist )
{
    using namespace Eigen;

    Eigen::Vector6d result;

    result.head<3>() =   transform             * twist.head<3>();
    //result.tail<3>() = ( transform.rotation()  * eulerXyzToRot( twist.tail<3>() ) ).eulerAngles(XAxisIndex,YAxisIndex,ZAxisIndex); //FIXME unknown if this is correct
                                                                                                   //According to ajs convention in somewhere

    Eigen::Affine3d rotation = ( transform.rotation()  * eulerXyzToRot( twist.tail<3>() ) ); // FIXME maybe do this different?

    result.tail<3>() = rotation.rotation().eulerAngles(XAxisIndex,YAxisIndex,ZAxisIndex);

    return result;
}


const KDL::TreeElement *
CartesianConstraint::
convertToNewBase( const std::string & baseElementFrameId, const Eigen::VectorXd & jointPositions )
{
    const KDL::TreeElement * oldBaseElement = m_baseElement;
    const KDL::TreeElement * newBaseElement = setBaseElement( baseElementFrameId );

    if(    newBaseElement != NULL
        && oldBaseElement != NULL )
    {
        std::string     currentBaseFrameId = oldBaseElement->segment.getName();
        std::string     newBaseFrameId     = newBaseElement->segment.getName();

        if( currentBaseFrameId != newBaseFrameId )
        {
            Eigen::VectorXd goalInCurrentBase  = goal();

            //FIXME this function is counterintuitive
            Eigen::Affine3d currentBaseToNewBase = getTransform( *m_fkSolver, newBaseFrameId, currentBaseFrameId, jointPositions );
            Eigen::VectorXd goalInNewBase( goalInCurrentBase.size() );

            //FIXME I'm continuously confused about order
            //FIXME I'm continuously confused about order
            goalInNewBase.head(3) = currentBaseToNewBase * Eigen::Vector3d( goalInCurrentBase.head(3) );

            if( goalInCurrentBase.size() == 6 )
                goalInNewBase.tail(3) = currentBaseToNewBase.rotation() * goalInCurrentBase.tail(3);


//            if( goalInCurrentBase.size() == 6 )
//                goalInNewBase = transformTwist( currentBaseToNewBase, goalInCurrentBase  );
//            else
//                goalInNewBase.head(3) = currentBaseToNewBase * Eigen::Vector3d( goalInCurrentBase.head(3) );
//



            setGoal( goalInNewBase );
        }
    }

    return m_baseElement;
}


}


