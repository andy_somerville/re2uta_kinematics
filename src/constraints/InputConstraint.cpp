/*
 * InputConstraint.cpp
 *
 *  Created on: Apr 23, 2013
 *      Author: andrew.somerville
 */


#include <re2uta/constraints/InputConstraint.hpp>
#include <kdl/tree.hpp>
#include <Eigen/Core>
#include <boost/assert.hpp>

namespace re2uta
{

InputConstraint::
InputConstraint( int startRow, int dimensions )
    : SolverConstraint( startRow, dimensions )
{
    m_dimensions  = dimensions;
    m_goalWeights = Eigen::MatrixXd::Identity(dimensions,dimensions);
}

InputConstraint::
~InputConstraint() {}

Eigen::MatrixXd
InputConstraint::
generateJacobian( const Eigen::VectorXd & currentInput )
{
    BOOST_ASSERT_MSG( m_dimensions == currentInput.size(), "input doesn't match dimensions" );
    Eigen::MatrixXd jacobian = Eigen::MatrixXd::Identity( m_dimensions, m_dimensions );
    return jacobian;
}

Eigen::VectorXd
InputConstraint::
getCurrentOutput( const Eigen::VectorXd & currentInput )
{
    return currentInput;
}

bool
InputConstraint::
solutionReached(  const Eigen::VectorXd & fullCurrentOutput, int iterations )
{
    return true;
}

int
InputConstraint::
dimensions() const { return m_dimensions; }

}
