///*
// * standard_jacobian.cpp
// *
// *  Created on: Mar 1, 2013
// *      Author: andrew.somerville
// */
//
//
//
//
//
//
//void calcStdJac_internal( const Eigen::Affine3d &  toLowerFrame,
//                          const Eigen::Affine3d &  toBaseFrame,
//                          const Eigen::Vector3d &  jointLocation,
//                          const KdlElementChildren & children,
//                          const KDL::TreeElement & element,
//                          const Eigen::VectorXd &  jointAngles,
//                          Eigen::MatrixXd &        jacobian_out,
//                          const Eigen::Vector3d &  jointAxis,
//                          const int                jointIndex,
//                          const std::string &      frameName,
//                          std::vector<bool> &      visited_out )
//{
//    BOOST_FOREACH( const KDL::SegmentMap::const_iterator & itor, children )
//    {
//        const KDL::TreeElement & childElement = itor->second;
//
//        if(    childElement.segment.getJoint().getType() == KDL::Joint::None  // FIXME this hack to get around KDL stupidity will likely cause an infinite loop eventually
//            || visited_out[ childElement.q_nr ] )
//        {
//            continue;
//        }
//
//        Eigen::Vector4d childPostPointMass(0,0,0,0);
//        calcStdJac( toBaseFrame, childElement, jointAngles, jacobian_out, visited_out );
//    }
//
//
//    if(    element.segment.getJoint().getType() != KDL::Joint::None
//        && !visited_out[ element.parent->second.q_nr]                 )
//    {
//        Eigen::Vector4d childPostPointMass(0,0,0,0);
//        calcStdJacBackward( toBaseFrame, element, jointAngles, jacobian_out, visited_out );
//    }
//
//    Eigen::Vector3d jointAxisInBase;
//    Eigen::Vector3d velocityInLocal;
//    Eigen::Vector3d velocityInBase;
//    Eigen::Vector3d tipInLocal;
//
//    velocityInLocal = jointAxis.cross( tipInLocal ); // Instantaneous velocity of target due to rotational velocity of joint
//    velocityInBase  = toBaseFrame.rotation() * velocityInLocal;
//
//    assert( jacobian_out.cols() > jointIndex );
//
//    jacobian_out.col( jointIndex ) = velocityInBase;
//
//    return;
//}
//
//
//
//
//
//
//
//void calcStdJacBackward( const Eigen::Affine3d &  lastToBaseFrame,
//                         const KDL::TreeElement & prevElement,
//                         const Eigen::VectorXd &  jointAngles,
//                         Eigen::MatrixXd &        jacobian_out,
//                         std::vector<bool> &      visited_out )
//
//{
//    const KDL::TreeElement & element      = prevElement.parent->second;
//    const KDL::Segment &     prevSegment  = prevElement.segment;
//    const KDL::Segment &     segment      = element.segment;
//    const KDL::Joint   &     joint        = prevSegment.getJoint();
//    const KDL::Joint   &     nextJoint    = segment.getJoint();
//    Eigen::Vector3d          jointAxis    = Eigen::Map< Eigen::Vector3d >( joint.JointAxis().data) ;
//    int                      jointIndex   = prevElement.q_nr;
//
//
//    if( joint.getType() != KDL::Joint::None  )
//    {
//        visited_out[ jointIndex ] = true;
//    }
//
//    Eigen::Vector3d jointOffset = Eigen::Vector3dMap( joint.pose(0).p.data );
//    Eigen::Affine3d toBaseFrame = lastToBaseFrame;
//    Eigen::Affine3d translation;
//    Eigen::Affine3d rotation;
//    Eigen::Affine3d toLowerFrame;
//
//    translation   = Eigen::Translation3d( -jointOffset );
//    rotation      = Eigen::AngleAxisd( -jointAngles( jointIndex ), jointAxis );
//    toLowerFrame  = rotation * translation;
//    toBaseFrame   = (toBaseFrame * toLowerFrame);
//
//    Eigen::Vector4d localPointMass;
//    localPointMass.head<3>()    = Eigen::Vector3dMap( segment.getInertia().getCOG().data );
//    localPointMass.tail<1>()(0) = segment.getInertia().getMass();
//
//
//    calcStdJac_internal( toLowerFrame,
//                         toBaseFrame,
//                         jointOffset,
//                         element.children,
//                         element,
//                         jointAngles,
//                         jacobian_out,
//                         jointAxis,
//                         jointIndex,
//                         segment.getName(),
//                         visited_out );
//
//    return;
//}
//
//
//void calcStdJac( const Eigen::Affine3d &  lastToBaseFrame,
//                 const KDL::TreeElement & element,
//                 const Eigen::VectorXd &  jointAngles,
//                 Eigen::MatrixXd &        jacobian_out,
//                 std::vector<bool> &      visited_out )
//{
//    const KDL::Segment &  segment    = element.segment;
//    const KDL::Joint   &  joint      = segment.getJoint();
//    Eigen::Vector3d       jointAxis  = Eigen::Map< Eigen::Vector3d >( joint.JointAxis().data) ;
//    int                   jointIndex = element.q_nr;
//
//    if( joint.getType()  != KDL::Joint::None )
//        visited_out[ jointIndex ] = true;
//
//    Eigen::Vector3d jointOffset = Eigen::Vector3dMap( joint.pose(jointAngles(jointIndex)).p.data ); //FIXME warning this may not match my concept for what joint offset is
//
//    Eigen::Affine3d toBaseFrame = lastToBaseFrame;
//    Eigen::Affine3d toLowerFrame;
//    toLowerFrame = Eigen::Translation3d( jointOffset );
//    toLowerFrame = toLowerFrame * Eigen::AngleAxisd( jointAngles( jointIndex ), jointAxis ); // note post-multiply means reverse rotation
//
//    toBaseFrame  = toBaseFrame * toLowerFrame;
//
//
//    Eigen::Vector4d localPointMass;
//    localPointMass.head<3>()    = Eigen::Vector3dMap( segment.getInertia().getCOG().data );
//    localPointMass.tail<1>()(0) = segment.getInertia().getMass();
//
//
//    calcStdJac_internal( toLowerFrame,
//                      toBaseFrame,
//                      Eigen::Zero3d,
//                      element.children,
//                      element,
//                      jointAngles,
//                      jacobian_out,
//                      jointAxis,
//                      jointIndex,
//                      segment.getName(),
//                      visited_out );
//}
