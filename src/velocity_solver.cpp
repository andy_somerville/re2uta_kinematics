/*
 * velocity_solver.cpp
 *
 *  Created on: Nov 27, 2012
 *      Author: somervil
 */

#include <Eigen/SVD>
#include <Eigen/Core>
#include <Eigen/Geometry>

// with help from http://eigen.tuxfamily.org/index.php?title=FAQ#Is_there_a_method_to_compute_the_.28Moore-Penrose.29_pseudo_inverse_.3F
template<typename MatrixT>
Eigen::Matrix<double, MatrixT::ColsAtCompileTime, 1 >
computeOutputDirectionInInputSpace( const MatrixT & jacobianMat,
                                    const Eigen::Matrix<double, MatrixT::RowsAtCompileTime,1> & goalDirection )
{
    typedef typename Eigen::JacobiSVD<MatrixT> JacobiSVD;
    JacobiSVD svd;
    svd.compute(jacobianMat, Eigen::ComputeFullU | Eigen::ComputeFullV );

    typename JacobiSVD::MatrixUType        uMat = svd.matrixU();
    typename JacobiSVD::MatrixVType        vMat = svd.matrixV();

    typename JacobiSVD::SingularValuesType sMat = svd.singularValues();
    typename JacobiSVD::SingularValuesType sMatPinv = sMat;

    double epsilon = 1.e-6; // taken from example
    for (long i = 0; i < jacobianMat.cols(); ++i)
    {
        if (sMat(i) > epsilon)
            sMatPinv(i) = 1.0 / sMat(i);
        else
            sMatPinv(i) = 0;
    }

    typename JacobiSVD::MatrixType pinvMat;
    pinvMat = (vMat * sMatPinv.asDiagonal() * uMat.transpose());

    Eigen::Matrix<double,MatrixT::ColsAtCompileTime, 1> resultVector;

    resultVector = pinvMat * goalDirection;

    return resultVector;
}



template <typename MatrixT>
typename Eigen::Matrix<typename MatrixT::Scalar, MatrixT::ColsAtCompileTime, MatrixT::RowsAtCompileTime>
moorePenrosePsudoInverse( const MatrixT & inputMat )
{
    typedef typename Eigen::JacobiSVD<MatrixT> JacobiSVD;
    JacobiSVD svd;
    svd.compute(inputMat, Eigen::ComputeFullU | Eigen::ComputeFullV );

    typename JacobiSVD::MatrixUType        uMat     = svd.matrixU();
    typename JacobiSVD::MatrixVType        vMat     = svd.matrixV();

    typename JacobiSVD::SingularValuesType sMat     = svd.singularValues();
    typename JacobiSVD::SingularValuesType sMatPinv = sMat;

    double epsilon = 1.e-6; // taken from example
    for (long i = 0; i < inputMat.cols(); ++i)
    {
        if (sMat(i) > epsilon)
            sMatPinv(i) = 1.0 / sMat(i);
        else
            sMatPinv(i) = 0;
    }

    typedef typename Eigen::Matrix<typename MatrixT::Scalar, MatrixT::ColsAtCompileTime, MatrixT::RowsAtCompileTime> PinvMat;
    PinvMat pinvMat;

    PinvMat singularValuesCorrected;
    singularValuesCorrected = PinvMat::Zero();
    int minDim = std::min(MatrixT::ColsAtCompileTime, MatrixT::RowsAtCompileTime);
    singularValuesCorrected.block( 0,0,minDim,minDim ) = sMatPinv.asDiagonal();

    pinvMat = (vMat * singularValuesCorrected * uMat.transpose());

    return pinvMat;
}


//void pinv(MatrixType& pinvmat)
//{
//    ei_assert(m_isInitialized && "SVD is not initialized.");
//
//    double pinvtoler = 1.e-6; // choose your tolerance wisely!
//    SingularValuesType m_sigma_inv = m_sigma;
//
//    for (long i = 0; i < m_workMatrix.cols(); ++i)
//    {
//        if (m_sigma(i) > pinvtoler)
//            m_sigma_inv(i) = 1.0 / m_sigma(i);
//        else
//            m_sigma_inv(i) = 0;
//    }
//
//    pinvmat = (m_matV * m_sigma_inv.asDiagonal() * m_matU.transpose());
//}
//
//
//

//int ChainIkSolverVel_pinv::CartToJnt(const JntArray& q_in, const Twist& v_in, JntArray& qdot_out)
//{
//    //Let the ChainJntToJacSolver calculate the jacobian "jac" for
//    //the current joint positions "q_in"
//    jnt2jac.JntToJac(q_in,jac);
//
//    //Do a singular value decomposition of "jac" with maximum
//    //iterations "maxiter", put the results in "U", "S" and "V"
//    //jac = U*S*Vt
//    int ret = svd.calculate(jac,U,S,V,maxiter);
//
//    double sum;
//    unsigned int i,j;
//
//    // We have to calculate qdot_out = jac_pinv*v_in
//    // Using the svd decomposition this becomes(jac_pinv=V*S_pinv*Ut):
//    // qdot_out = V*S_pinv*Ut*v_in
//
//    //first we calculate Ut*v_in
//    for (i=0;i<jac.columns();i++) {
//        sum = 0.0;
//        for (j=0;j<jac.rows();j++) {
//            sum+= U[j](i)*v_in(j);
//        }
//        //If the singular value is too small (<eps), don't invert it but
//        //set the inverted singular value to zero (truncated svd)
//        tmp(i) = 0;
//        if( fabs(S(i)) > eps )
//            temp(i) = sum * 1.0/S(i);
//
////        tmp(i) = sum*(fabs(S(i))<eps?0.0:1.0/S(i));
//    }
//    //tmp is now: tmp=S_pinv*Ut*v_in, we still have to premultiply
//    //it with V to get qdot_out
//    for (i=0;i<jac.columns();i++) {
//        sum = 0.0;
//        for (j=0;j<jac.columns();j++) {
//            sum+=V[i](j)*tmp(j);
//        }
//        //Put the result in qdot_out
//        qdot_out(i)=sum;
//    }
//    //return the return value of the svd decomposition
//    return ret;
//}
