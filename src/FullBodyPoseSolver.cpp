/*
 * FullBodyPoseSolver.cpp
 *
 *  Created on: Mar 22, 2013
 *      Author: andrew.somerville
 */

#include <re2uta/constraints/CenterOfMassConstraint.hpp>
#include <re2uta/constraints/FrameConstraint.hpp>
#include <re2uta/constraints/InputConstraint.hpp>
#include <re2uta/FullBodyPoseSolver.hpp>
#include <re2uta/gradient_descent.hpp>
#include <re2uta/tip_jacobian.hpp>
#include <re2uta/calc_point_mass.hpp>
#include <re2uta/center_of_mass_jacobian.hpp>
#include <re2uta/tip_jacobian.hpp>
#include <re2/kdltools/kdl_tree_util.hpp>
#include <re2/kdltools/kdl_tools.h>
#include <re2/eigen/eigen_util.h>

#include <re2uta/AtlasLookup.hpp>

namespace
{
    double nonNaN( const double & value )
    {
        if( std::isinf(value) || std::isnan(value) )
            return 0;
        else
            return value;
    }
}


namespace re2uta
{


FullBodyPoseSolver::
FullBodyPoseSolver( const urdf::Model & model,
                    const std::string & baseSegmentName,
                    const std::string & oppFootSegmentName,
                    const std::string & postureSegmentName ) 
{
    re2::kdltools::JointNameToAngleMap jointMinsMap;
    re2::kdltools::JointNameToAngleMap jointMaxsMap;

    re2::kdltools::getJointLimits( model, jointMinsMap, jointMaxsMap );

    KdlTreePtr tree( new KDL::Tree );

    kdlRootInertiaWorkaround( model, *tree );

    Eigen::VectorXd jointMins = Eigen::VectorXd::Ones( tree->getNrOfJoints() ) *      std::numeric_limits<double>::infinity();
    Eigen::VectorXd jointMaxs = Eigen::VectorXd::Ones( tree->getNrOfJoints() ) * -1 * std::numeric_limits<double>::infinity();

    int index = 0;
    BOOST_FOREACH( KDL::SegmentMap::value_type const & entry, tree->getSegments() )
    {
        KDL::Segment const & segment = entry.second.segment;

        re2::kdltools::JointNameToAngleMap::iterator minEntry;
        minEntry = jointMinsMap.find( segment.getJoint().getName() );
        if( minEntry != jointMinsMap.end() )
        {
            jointMins( entry.second.q_nr ) = minEntry->second;
        }

        re2::kdltools::JointNameToAngleMap::iterator maxEntry;
        maxEntry = jointMaxsMap.find( segment.getJoint().getName() );
        if( maxEntry != jointMaxsMap.end() )
        {
            jointMaxs( entry.second.q_nr ) = maxEntry->second;
        }

        ++index;
    }

    init( tree, jointMins, jointMaxs, baseSegmentName, oppFootSegmentName, postureSegmentName );
}

FullBodyPoseSolver::
FullBodyPoseSolver( const KDL::Tree & treeArg,
                    const Eigen::VectorXd & minInputValues,
                    const Eigen::VectorXd & maxInputValues,
                    const std::string &     baseSegmentName, 
                    const std::string &     oppFootSegmentName,
                    const std::string &     postureSegmentName )
{
    KdlTreePtr tree( new KDL::Tree );
    *tree = treeArg;
    init( tree, minInputValues, maxInputValues, baseSegmentName, oppFootSegmentName, postureSegmentName );
}



void
FullBodyPoseSolver::init( const KdlTreeConstPtr & tree,
                          const Eigen::VectorXd & inputMinValues,
                          const Eigen::VectorXd & inputMaxValues,
                          const std::string &     baseSegmentName, 
                          const std::string &     oppFootSegmentName,
                          const std::string &     postureSegmentName )
{
    m_tree           = tree;
    m_inputMinValues = inputMinValues;
    m_inputMaxValues = inputMaxValues;


    m_fkSolver.reset( new KDL::TreeFkSolverPos_recursive( *m_tree ) );

//    std::string baseSegmentName(    "l_foot" );
//    std::string oppFootSegmentName( "r_foot" );
//    std::string postureSegmentName( "utorso" );
////    std::string postureSegmentName( "pelvis" );

    const KDL::TreeElement * baseElement    = NULL;
    const KDL::TreeElement * oppFootElement = NULL;

    try
    {
        baseElement        = &getTreeElement( *m_tree, baseSegmentName    );
        oppFootElement     = &getTreeElement( *m_tree, oppFootSegmentName );
        m_postureElement   = &getTreeElement( *m_tree, postureSegmentName );
    }
    catch( ... )
    {
        throw;
    }


    m_numJoints = m_tree->getNrOfJoints();

    int startRow = 0;

    m_comConstraint.reset(         new CenterOfMassConstraint( startRow, m_tree, baseElement                     ) );
    m_solverConstraints.push_back( m_comConstraint );
    startRow += m_solverConstraints.back()->dimensions();

    m_oppFootConstraint.reset(     new FrameConstraint(        startRow, m_tree, baseElement,   oppFootElement, m_fkSolver ) );
    m_solverConstraints.push_back( m_oppFootConstraint );
    startRow += m_solverConstraints.back()->dimensions();

    m_postureConstraint.reset(     new FrameConstraint(        startRow, m_tree, baseElement, m_postureElement, m_fkSolver ) );
    m_solverConstraints.push_back( m_postureConstraint );
    m_postureConstraint->goalWeightVector().head(3) = Eigen::Zero3d;
    startRow += m_solverConstraints.back()->dimensions();

    m_optimalAngleConstraint.reset( new InputConstraint(       startRow, m_numJoints ) );
    m_solverConstraints.push_back( m_optimalAngleConstraint );
    startRow += m_solverConstraints.back()->dimensions();

    m_goalDims = 0;
    BOOST_FOREACH( SolverConstraint::Ptr constraint, m_solverConstraints )
    {
        m_goalDims += constraint->dimensions();
    }
}


void
FullBodyPoseSolver::
setDefaultJointPose( const Eigen::VectorXd & jointPositions )
{
    m_optimalAngleConstraint->setGoal( jointPositions );
}


FrameConstraint::Ptr
FullBodyPoseSolver::
addFrameConstraint( const std::string & baseElementName, const std::string & tipElementName  )
{
    const KDL::TreeElement * baseElement;
    const KDL::TreeElement * tipElement;
    baseElement = &getTreeElement( *m_tree, baseElementName );
    tipElement  = &getTreeElement( *m_tree,  tipElementName );

    return addFrameConstraint( baseElement, tipElement );
}

FrameConstraint::Ptr
FullBodyPoseSolver::
addFrameConstraint( const KDL::TreeElement * baseElement, const KDL::TreeElement * tipElement )
{
    FrameConstraint::Ptr frameConstraint;

    if( baseElement != NULL && tipElement != NULL )
    {
        int startRow = m_goalDims;
        frameConstraint.reset( new FrameConstraint( startRow, m_tree, baseElement, tipElement, m_fkSolver ) );
        m_solverConstraints.push_back( frameConstraint );
        frameConstraint->goalWeightVector() = Eigen::Vector6d::Zero();

        m_goalDims += frameConstraint->dimensions();
    }
    else
    {
        ROS_ERROR( "Tree element for frame constraint null: base: %x  tip: %x", baseElement, tipElement );
    }

    return frameConstraint;
}

bool
FullBodyPoseSolver::
removeFrameConstraint( const FrameConstraint::Ptr & frameConstraint )
{
    SolverConstraintList::iterator element;
    element = std::find( m_solverConstraints.begin(), m_solverConstraints.end(), frameConstraint );

    if( element == m_solverConstraints.end() )
    {
        ROS_ERROR( "Attempt to remove frame constraint which isn't in list" );
        return false;
    }

    m_solverConstraints.erase( element );

    int startRow = 0;
    BOOST_FOREACH( SolverConstraint::Ptr constraint, m_solverConstraints )
    {
        constraint->setStartRow( startRow );
        startRow += constraint->dimensions();
    }
    m_goalDims = startRow;

    return frameConstraint;
}

Eigen::VectorXd
FullBodyPoseSolver::
solvePose(const std::string &     baseElementName,
          const std::string &     oppositeFootName,
          const Eigen::VectorXd & jointPositions,
          const Eigen::Vector6d & desiredOppositeFootPose,
          const Eigen::Vector3d & desiredPostureOrientation,
          const Eigen::Vector3d & desiredComPose,
          const Eigen::VectorXd & desiredJointPose )
{
    m_goalDims = 0;
    BOOST_FOREACH( SolverConstraint::Ptr constraint, m_solverConstraints )
    {
        m_goalDims += constraint->dimensions();
    }


    const KDL::TreeElement * baseElement;
    const KDL::TreeElement * oppositeFootElement;

    try
    {
        baseElement         = &getTreeElement( *m_tree, baseElementName  );
        oppositeFootElement = &getTreeElement( *m_tree, oppositeFootName );
    }
    catch( ... )
    {
        return Eigen::VectorXd();
    }

    m_comConstraint->      setBaseElement( baseElement ); // FIXME this should be handled in a loop, but requires increased interface complexity
    m_postureConstraint->  setBaseElement( baseElement ); // FIXME this should be handled in a loop, but requires increased interface complexity
    m_oppFootConstraint->  setBaseElement( baseElement ); // FIXME this should be handled in a loop, but requires increased interface complexity
//    BOOST_FOREACH( SolverConstraint::Ptr constraint, m_solverConstraints )
//    {
//        constraint->setBaseElement( baseElement );
//    }

    m_oppFootConstraint->setTipElement( oppositeFootElement );

    Eigen::VectorXd desiredPosturePose = Eigen::VectorXd::Zero(6);
    desiredPosturePose.tail(3) = desiredPostureOrientation;

    m_comConstraint->         setGoal( desiredComPose           );
    m_oppFootConstraint->     setGoal( desiredOppositeFootPose  );
    m_postureConstraint->     setGoal( desiredPosturePose       );
    m_optimalAngleConstraint->setGoal( desiredJointPose         );

    m_comConstraint->goalWeightVector()(0)        = 0.5;
//    m_comConstraint->getGoalWeightVector()(1)        = 0.1;
    m_comConstraint->goalWeightVector()(2)        = 0.02;

    m_postureConstraint->goalWeightVector()       = Eigen::VectorXd::Zero(6);
    m_postureConstraint->goalWeightVector()(3)    = 0.0001; // rot x
    m_postureConstraint->goalWeightVector()(4)    = 0.001;  // rot y
    m_postureConstraint->goalWeightVector()(5)    = 0.0001; // rot z
    m_optimalAngleConstraint->goalWeightVector()  = Eigen::VectorXd::Ones(m_optimalAngleConstraint->dimensions()) * 0.02;

    return solvePose( jointPositions );
}



Eigen::VectorXd
FullBodyPoseSolver::
solvePose( const Eigen::VectorXd & jointPositions, int maxIterations )
{
    m_goalDims = 0;
    BOOST_FOREACH( SolverConstraint::Ptr constraint, m_solverConstraints )
    {
        m_goalDims += constraint->dimensions();
    }

    Eigen::VectorXd desiredOutput( m_goalDims );

    int atRow = 0;
    BOOST_FOREACH( SolverConstraint::Ptr constraint, m_solverConstraints )
    {
        ROS_ASSERT_MSG( constraint->goal().size() == constraint->dimensions(), "Solver constraint does not match expected dimenstions" );
        atRow = re2::insertRowsAt( desiredOutput, constraint->goal(), atRow );
    }

    Eigen::MatrixXd inputWeights; // intentionally empty so gradient descent solver will ignore
    Eigen::MatrixXd outputWeights = Eigen::VectorXd::Ones(m_goalDims).asDiagonal();

    int atDiag = 0;
    BOOST_FOREACH( SolverConstraint::Ptr constraint, m_solverConstraints )
    {
        ROS_ASSERT_MSG( constraint->goalWeightVector().size() == constraint->dimensions(), "Solver weight matrix does not match expected dimenstions" );
        atDiag = re2::insertBlockAt( outputWeights, constraint->goalWeightMatrix(), atDiag );
    }

    Eigen::Affine3d defaultTransform = Eigen::Affine3d::Identity();
    FkSolveFunctor  solveFk;
    JacobianFunctor generateJacobian;
    GuardFunctor    isCloseEnough;

    using boost::ref;
    solveFk          = boost::bind( &FullBodyPoseSolver::calcCurrentPose,    this, _1, ref( defaultTransform ) );
    generateJacobian = boost::bind( &FullBodyPoseSolver::calcMasterJacobian, this, _1, ref( defaultTransform ) );
    isCloseEnough    = boost::bind( &FullBodyPoseSolver::shouldStopSolving,  this, _1, _2, _3, maxIterations );

    Eigen::VectorXd inputSpaceSolution;
    inputSpaceSolution = solveGradientDescent( jointPositions,
                                               generateJacobian,
                                               solveFk,
                                               desiredOutput,
                                               isCloseEnough,
                                               m_inputMinValues,
                                               m_inputMaxValues,
                                               m_optimalAngleConstraint->goal(),
                                               inputWeights,
                                               outputWeights );
    return inputSpaceSolution;
}



// functionality from gradient descent should be baked into any cartesian controller which uses this velocity solver
// to avoid joint limit problems.
Eigen::VectorXd
FullBodyPoseSolver::
solveVelocity( const Eigen::VectorXd & jointPositions, const Eigen::VectorXd & desiredOutputVelocity )
{
    Eigen::MatrixXd inputWeights; // intentionally empty so gradient descent solver will ignore
    Eigen::MatrixXd outputWeights = Eigen::VectorXd::Ones(m_goalDims).asDiagonal();

    int atDiag = 0;
    BOOST_FOREACH( SolverConstraint::Ptr constraint, m_solverConstraints )
    {
        atDiag = re2::insertBlockAt( outputWeights, constraint->goalWeightMatrix(), atDiag );
    }

    Eigen::Affine3d defaultTransform = Eigen::Affine3d::Identity();
    Eigen::MatrixXd masterJacobian   = calcMasterJacobian( jointPositions, defaultTransform );

    double lambda = 0.1;
    Eigen::MatrixXd pinvMasterJacobian = weightedPseudoInverse( masterJacobian, inputWeights, outputWeights, lambda );

    Eigen::VectorXd jointVelocities    = pinvMasterJacobian * desiredOutputVelocity;

    return jointVelocities;
}





Eigen::MatrixXd
FullBodyPoseSolver::
calcMasterJacobian(const Eigen::VectorXd &  jointPositions,
                   const Eigen::Affine3d &  defaultTransform  )
{
    Eigen::MatrixXd masterJacobian = Eigen::MatrixXd::Ones(m_goalDims,m_numJoints) * std::numeric_limits<double>::quiet_NaN();;

    int atRow = 0;
    BOOST_FOREACH( SolverConstraint::Ptr constraint, m_solverConstraints )
    {
        atRow = re2::insertRowsAt( masterJacobian, constraint->generateJacobian(jointPositions), atRow );
    }

    return masterJacobian;
}


Eigen::VectorXd
FullBodyPoseSolver::
calcCurrentPose( const Eigen::VectorXd &  jointPositions,
                 const Eigen::Affine3d &  defaultTransform )// FIXME not used yet
{
    Eigen::VectorXd totalPose = Eigen::VectorXd::Ones(m_goalDims) * std::numeric_limits<double>::quiet_NaN();

    int atRow = 0;
    BOOST_FOREACH( SolverConstraint::Ptr constraint, m_solverConstraints )
    {
        atRow = re2::insertRowsAt( totalPose, constraint->getCurrentOutput( jointPositions ), atRow );
    }

    return totalPose;
};


Eigen::VectorXd
FullBodyPoseSolver::
calcCurrentVelocity( const Eigen::VectorXd &  jointPositions,
                     const Eigen::VectorXd &  jointVelocities,
                     const Eigen::Affine3d &  defaultTransform )// FIXME not used yet
{
    Eigen::VectorXd velocity(m_goalDims);

    Eigen::MatrixXd jacobian = calcMasterJacobian( jointPositions, defaultTransform );

    velocity = jacobian * jointVelocities;

    return velocity;
};


bool
FullBodyPoseSolver::
shouldStopSolving( const Eigen::VectorXd & currentOutputPose, const Eigen::VectorXd & targetOutputPose, int iterations, int maxIterations )
{
    bool stopSolving = true;

    if(    iterations > maxIterations
        || ! ros::ok() )
    {
        stopSolving = true;
    }
    else
    {
        BOOST_FOREACH( SolverConstraint::Ptr constraint, m_solverConstraints )
        {
            stopSolving = stopSolving && constraint->solutionReached( currentOutputPose, iterations );
        }
    }

    return stopSolving;
}



Eigen::Affine3d
FullBodyPoseSolver::
getTransformFromTo( const std::string & baseFrame,
                    const std::string & tipFrame,
                    const Eigen::VectorXd & jointPositions )
{
    return getTransform( *m_fkSolver, tipFrame, baseFrame, jointPositions );
}


Eigen::Vector6d
FullBodyPoseSolver::
getPoseTwist( const std::string & baseFrame,
         const std::string & tipFrame,
         const Eigen::VectorXd & jointPositions ) // FIXME should take default transform
{
    Eigen::Affine3d twistXform = getTransform( *m_fkSolver, baseFrame, tipFrame, jointPositions );

    Eigen::Vector6d twist;
    twist.head<3>() = twistXform.translation();
    twist.tail<3>() = twistXform.rotation().eulerAngles(0,1,2); //FIXME could this be a problem because of singularities??

    return twist;
}

Eigen::Affine3d
FullBodyPoseSolver::
getPose( const std::string & baseFrame,
         const std::string & tipFrame,
         const Eigen::VectorXd & jointPositions ) // FIXME should take default transform
{
    return getTransform( *m_fkSolver, baseFrame, tipFrame, jointPositions );
}

Eigen::Vector3d
FullBodyPoseSolver::
getCenterOfMass( const std::string & baseFrame,
                 const Eigen::VectorXd & jointPositions )
{
    Eigen::Vector4d currentPointMass;

    currentPointMass = calcPointMass( *m_tree, getTreeElement( *m_tree, baseFrame ), jointPositions );

    return currentPointMass.head<3>();
}



}



