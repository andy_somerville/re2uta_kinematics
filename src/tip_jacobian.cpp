#pragma once

#include <re2uta/debug.hpp>
#include <re2/kdltools/kdl_tree_util.hpp>
#include <re2uta/tip_jacobian.hpp>
#include <re2uta/kinematics.hpp>

#include <re2/eigen/eigen_util.h>

#include <ros/ros.h>

#include <eigen_conversions/eigen_msg.h>
#include <kdl/chainjnttojacsolver.hpp>
#include <kdl/segment.hpp>
#include <kdl/joint.hpp>
#include <kdl/tree.hpp>

#include <Eigen/Core>
#include <Eigen/Geometry>

#include <boost/shared_ptr.hpp>
//#include <boost/fusion/algorithm/query/find.hpp>
#include <boost/range/algorithm/find.hpp>


namespace re2uta
{


//JacobianPtr calculateJacobian( const KDL::Chain & chain, const KDL::JntArray & currentJointPositions, const std::vector<bool> & lockedJoints, const int numUnlockedJoints )
KDL::Jacobian
calculateTipJacobian( const KDL::Chain & chain,
                      const KDL::JntArray & currentJointPositions,
                      const std::vector<bool> & lockedJoints,
                      const int numUnlockedJoints,
                      const KDL::Frame & defaultFrame )
{
//    JacobianPtr jacobian;
    KDL::Jacobian jacobian;

    if( currentJointPositions.rows() != chain.getNrOfJoints() )
    {
        ROS_ERROR( "currentJointPositions.rows() != chain.getNrOfJoints(): bailing out of calculateTipJacobian" );
        return jacobian;
    }

//    jacobian.reset( new KDL::Jacobian );
//    jacobian->resize( numUnlockedJoints );
    jacobian.resize( numUnlockedJoints );

    KDL::Frame accumulatedFrame     = defaultFrame;
    KDL::Frame nextAccumulatedFrame = KDL::Frame::Identity();

    KDL::Twist velocity6d;
    SetToZero( velocity6d );

    unsigned int jointIndex         = 0;
    unsigned int unlockedJointIndex = 0;
    unsigned int segmentIndex       = 0;
    for( segmentIndex = 0;  segmentIndex < chain.getNrOfSegments() ; ++segmentIndex )
    {
        //Calculate new Frame_base_ee
        const KDL::Segment &          currentSegment      = chain.getSegment( segmentIndex );
        const KDL::Joint::JointType & currentJointType     = currentSegment.getJoint().getType();
        bool                          currentJointIsLocked = lockedJoints[jointIndex];

        if( currentJointType != KDL::Joint::None )
        {
            nextAccumulatedFrame = accumulatedFrame * currentSegment.pose( currentJointPositions(jointIndex) );
        }else
        {
            nextAccumulatedFrame = accumulatedFrame * currentSegment.pose( 0.0 );
        }

        //Changing Refpoint of all columns to new ee
        jacobian.changeRefPoint( nextAccumulatedFrame.p - accumulatedFrame.p );

        //Only increase jointnr if the segment has a joint
        if( currentJointType != KDL::Joint::None )
        {
            //Only put the twist inside if it is not locked
            if( ! currentJointIsLocked )
            {
                velocity6d = accumulatedFrame.M * currentSegment.twist( currentJointPositions(jointIndex), 1.0 );
                jacobian.setColumn( unlockedJointIndex, velocity6d );
                unlockedJointIndex++;
            }

            jointIndex++;
        }

        accumulatedFrame = nextAccumulatedFrame;
    }

    return jacobian;
}


KDL::Jacobian
calculateTipJacobian( const KDL::Chain & chain,
                      const Eigen::VectorXd & currentJointPositions,
                      const std::vector<bool> & lockedJoints,
                      const int numUnlockedJoints,
                      const KDL::Frame & defaultFrame )
{
    KDL::JntArray kdlJointPositions;
    kdlJointPositions.data = currentJointPositions;

    return calculateTipJacobian( chain, kdlJointPositions, lockedJoints, numUnlockedJoints, defaultFrame );
}


//Eigen::MatrixXd calculateJacobian( const KDL::Chain & chain, const Eigen::VectorXd & currentJointPositions )
//{
//    Eigen::MatrixXd jacobian;
//
//    if( currentJointPositions.rows() != chain.getNrOfJoints() )
//    {
//        return jacobian;
//    }
//
//    jacobian.resize( 6, chain.getNrOfJoints() );
//
//    Eigen::Affine3d accumulatedFrame     = Eigen::Affine3d::Identity();
//    Eigen::Affine3d nextAccumulatedFrame = Eigen::Affine3d::Identity();
//
//    Eigen::Matrix< double, 6, 1 > velocity6d;
//    velocity6d.array() = 0;
//
//    unsigned int jointIndex         = 0;
//    unsigned int segmentIndex       = 0;
//    for( segmentIndex = 0;  segmentIndex < chain.getNrOfSegments() ; ++segmentIndex )
//    {
//        //Calculate new Frame_base_ee
//        const KDL::Segment &          currentSegement      = chain.getSegment( segmentIndex );
//        const KDL::Joint::JointType & currentJointType     = currentSegement.getJoint().getType();
//        bool                          currentJointIsLocked = lockedJoints[jointIndex];
//
//        if( currentJointType != KDL::Joint::None )
//        {
//            nextAccumulatedFrame = accumulatedFrame * currentSegement.pose( currentJointPositions(jointIndex) );
//        }else
//        {
//            nextAccumulatedFrame = accumulatedFrame * currentSegement.pose( 0.0 );
//        }
//
//        //Changing Refpoint of all columns to new ee
//        jacobian.changeRefPoint( nextAccumulatedFrame.p - accumulatedFrame.p );
//
//        //Only increase jointnr if the segment has a joint
//        if( currentJointType != KDL::Joint::None )
//        {
//            //Only put the twist inside if it is not locked
//            if( ! currentJointIsLocked )
//            {
//                velocity6d = accumulatedFrame.M * currentSegement.twist( currentJointPositions(jointIndex), 1.0 );
//                jacobian.setColumn( unlockedJointIndex, velocity6d );
//                unlockedJointIndex++;
//            }
//            jointIndex++;
//        }
//
//        accumulatedFrame = nextAccumulatedFrame;
//    }
//
//    return jacobian;
//}


//JacobianPtr calculateJacobianFromCondensedChain( const KDL::Chain & condensedChain, const KDL::JntArray& currentJointPositions )
KDL::Jacobian
calculateTipJacobianFromCondensedChain( const KDL::Chain & condensedChain,
                                        const KDL::JntArray& currentJointPositions )
{
    KDL::Jacobian jacobian;

//    jacobian.reset( new KDL::Jacobian );
//    jacobian->resize( condensedChain.getNrOfJoints() );
    jacobian.resize( condensedChain.getNrOfJoints() );

    KDL::Frame accumulatedFrame     = KDL::Frame::Identity();
    KDL::Frame nextAccumulatedFrame = KDL::Frame::Identity();

    KDL::Twist velocity6dInBaseFrame;
    SetToZero( velocity6dInBaseFrame );

    unsigned int jointIndex         = 0;
    unsigned int segmentIndex       = 0;
    for( segmentIndex = 0;  segmentIndex < condensedChain.getNrOfSegments() ; ++segmentIndex )
    {
        //Calculate new Frame_base_ee
        const KDL::Segment &          currentSegment       = condensedChain.getSegment( segmentIndex );
        const double                  currentJointPosition = currentJointPositions(jointIndex);

        //pose of the new end-point expressed in the base
        nextAccumulatedFrame  = accumulatedFrame   * currentSegment.pose(  currentJointPosition      );
        velocity6dInBaseFrame = accumulatedFrame.M * currentSegment.twist( currentJointPosition, 1.0 );

        //Changing Refpoint of all columns to new ee
//        jacobian->changeRefPoint( nextAccumulatedFrame.p - accumulatedFrame.p );
        jacobian.changeRefPoint( nextAccumulatedFrame.p - accumulatedFrame.p );

//        jacobian->setColumn( jointIndex, velocity6dInBaseFrame );
        jacobian.setColumn( jointIndex, velocity6dInBaseFrame );
        jointIndex++;

        accumulatedFrame = nextAccumulatedFrame;
    }

    return jacobian;
}
//
//
//
//TwistVectorPtr calculateJacobianFromCondensedChain( const Chain & condensedChain, const JntArray& currentJointPositions )
//{
//    TwistVectorPtr twists;
//
//    twists.reset( new TwistVector );
//
//    Frame accumulatedFrame     = Frame::Identity();
//    Frame nextAccumulatedFrame = Frame::Identity();
//
//    Twist velocity6dInBaseFrame;
//    SetToZero( velocity6dInBaseFrame );
//
//    unsigned int jointIndex         = 0;
//    unsigned int segmentIndex       = 0;
//    for( segmentIndex = 0;  segmentIndex < chain.getNrOfSegments() ; ++segmentIndex )
//    {
//        //Calculate new Frame_base_ee
//        const Segement &  currentSegement      = chain.getSegment( segmentIndex );
//        const JointType & currentJointType     = currentSegement.getJoint().getType();
//        const double      currentJointPosition = currentJointPositions[jointIndex];
//
//        //pose of the new end-point expressed in the base
//        nextAccumulatedFrame  = accumulatedFrame   * currentSegement.pose(  currentJointPosition      );
//        velocity6dInBaseFrame = accumulatedFrame.M * currentSegement.twist( currentJointPosition, 1.0 );
//
//        twists->push_back( velocity6dInBaseFrame );
//        jointIndex++;
//
//        accumulatedFrame = nextAccumulatedFrame;
//    }
//
//    //Move all
//    BOOST_FOREACH( Twist & twist, twists )
//    {
//        twist.p += accumulatedFrame.p;
//    }
//
//    return twists;
//}
//
//
//TwistVectorPtr calculateTwistTree( const Tree & tree )
//{
//
//}




//TwistdVectorPtr calculateJacobian( const KDL::Chain & chain, const KDL::JntArray & currentJointPositions )
//{
////    JacobianPtr jacobian;
//    TwistdVectorPtr twistsInRootFrame;
//
//    if( currentJointPositions.rows() != chain.getNrOfJoints() )
//    {
//        return twistsInRootFrame;
//    }
//
//    twistsInRootFrame.reset( new TwistVectorPtr );
//
//    Eigen::Transform accumulatedFrame     = Eigen::Transform::Identity();
//    Eigen::Transform nextAccumulatedFrame = Eigen::Transform::Identity();
//
//    Twistd velocity6d;
//    velocity6d.translation = Eigen::Zero3d;
//    velocity6d.rotation    = Eigen::Zero3d;
//
//    unsigned int jointIndex         = 0;
////    unsigned int unlockedJointIndex = 0;
//    unsigned int segmentIndex       = 0;
//    for( segmentIndex = 0;  segmentIndex < chain.getNrOfSegments() ; ++segmentIndex )
//    {
//        //Calculate new Frame_base_ee
//        const KDL::Segment &          currentSegement      = chain.getSegment( segmentIndex );
//        const KDL::Joint::JointType & currentJointType     = currentSegement.getJoint().getType();
////        bool                          currentJointIsLocked = lockedJoints[jointIndex];
//
//        KDL::Frame currentKdlPose;
//        if( currentJointType != KDL::Joint::None )
//        {
//            currentKdlPose = currentSegement.pose( currentJointPositions(jointIndex) );
//        }else
//        {
//            currentKdlPose = currentSegement.pose( 0.0 );
//        }
//
//        Eigen::Transform currentSegmentTransform;
//        currentSegmentTransform.rotation()    = Eigen::Map<Eigen::Matrix3d>( currentKdlPose.M.data, 3, 3 );
//        currentSegmentTransform.translation() = Eigen::Map<Vector3d>(        currentKdlPose.p.data, 3    );
//
//        nextAccumulatedFrame = accumulatedFrame * currentSegmentTransform;
//
//        //Changing Refpoint of all columns to new ee
//        jacobian.changeRefPoint( nextAccumulatedFrame.p - accumulatedFrame.p );
//
//        //Only increase jointnr if the segment has a joint
//        if( currentJointType != KDL::Joint::None )
//        {
//            //Only put the twist inside if it is not locked
////            if( ! currentJointIsLocked )
//            {
//                KDL::Twist currentSegmentKdlTwist;
//                currentSegmentKdlTwist = currentSegment.twist( currentJointPositions(jointIndex), 1.0);
//
//                Twist currentSegmentTwist;
//                currentSegmentTwist.translation = Eigen::Map<Vector3d>( currentSegmentKdlTwist.vel, 3 );
//                currentSegmentTwist.rotation    = Eigen::Map<Vector3d>( currentSegmentKdlTwist.rot, 3 );
//
//                velocity6d = accumulatedFrame.rotation() * currentSegmentTwist;
//
//                twistsInRootFrame.push_back( velocity6d );
//            }
//            jointIndex++;
//        }
//
//        accumulatedFrame = nextAccumulatedFrame;
//    }
//
//    return twistsInRootFrame;
//}




Eigen::MatrixXd
calcTreeTipJacobian( const KDL::Tree &        tree,
                     const KDL::TreeElement * baseElement,
                     const KDL::TreeElement * tipElement,
                     const Eigen::VectorXd &  treeJointPositions,
                     const Eigen::Affine3d &  defaultTransform )
{
    std::string baseSegName = baseElement->segment.getName();
    std::string tipSegName  = tipElement->segment.getName();

    KDL::Chain chain;
    tree.getChain( baseSegName, tipSegName, chain );

    std::vector<bool> jointLockState( chain.getNrOfJoints(), false );
    int numUnlockedJoints = chain.getNrOfJoints();

//    ROS_INFO_STREAM( "Chain from " << baseSegName << " to " << tipSegName << " has " << chain.getNrOfSegments() << " segments and " << chain.getNrOfJoints() << " joints" );
    Eigen::VectorXd chainJointPositions( chain.getNrOfJoints() );

    int direction = -1; // if the baseElement is pelvis then this should start as 1 instead

    std::string lastChainSegName = baseSegName;
    for( int chainJoint = 0; chainJoint < chainJointPositions.rows(); ++chainJoint )
    {
        const KDL::Segment & chainSegment = chain.getSegment( chainJoint );
        const std::string &  chainSegName = chainSegment.getName();

        std::string treeSegName;

        if( direction == -1 )
            treeSegName = lastChainSegName;
        else
            treeSegName = chainSegName;

        const KDL::TreeElement & element = tree.getSegment( treeSegName )->second;

//        ROS_INFO_STREAM( "" );
//        ROS_INFO_STREAM( "direction           " << direction );
//        ROS_INFO_STREAM( "chainJoint number   " << chainJoint );
//        ROS_INFO_STREAM( "last chain segment  " << lastChainSegName );
//        ROS_INFO_STREAM( "chain segment       " << chainSegName );
//        ROS_INFO_STREAM( "tree segment        " << element.segment.getName() );
//        ROS_INFO_STREAM( "chain segment joint " << chainSegment.getJoint().getName() );
//        ROS_INFO_STREAM( "tree segment joint  " << element.segment.getJoint().getName() );
//        ROS_INFO_STREAM( "chain joint axis    " << Eigen::Vector3dConstMap( chainSegment.getJoint().JointAxis().data    ).transpose() );
//        ROS_INFO_STREAM( "tree joint axis     " << Eigen::Vector3dConstMap( element.segment.getJoint().JointAxis().data ).transpose() );
//
//        ROS_INFO_STREAM( "joint offset   " << Eigen::Vector3dMap( chain.getSegment( chainJoint ).getJoint().pose(0).p.data ).transpose() );
//        ROS_INFO_STREAM( "segment frameToTip  " << Eigen::Vector3dMap( chain.getSegment( chainJoint ).getFrameToTip().p.data ).transpose() );
//        ROS_INFO_STREAM( "qnr: " <<  element.q_nr );
//        ROS_INFO_STREAM( "tree jointAngle: " <<  treeJointPositions( element.q_nr ) );

//        chainJointPositions(chainJoint) = direction * treeJointPositions( element.q_nr );
        chainJointPositions(chainJoint) = treeJointPositions( element.q_nr );

        if( tree.getSegment( chainSegName )->second.segment.getJoint().getType() == KDL::Joint::None ) // FIXME this needs to be verfied if it should be here or before the assignment
            direction = 1;

        BOOST_FOREACH( const KDL::SegmentMap::const_iterator & childItor, tree.getSegment( chainSegName )->second.children  )
        {
            if(    chainJoint + 1 < chainJointPositions.rows()
                && childItor->first == chain.getSegment( chainJoint + 1 ).getName() )
            {
                direction = 1;
            }
        }

        lastChainSegName = chainSegName;
    }

    KDL::Frame defaultFrame;
    transformEigenToKdl( defaultTransform, defaultFrame );

    KDL::Jacobian kdlJacobian;
    kdlJacobian = calculateTipJacobian( chain, chainJointPositions, jointLockState, numUnlockedJoints, defaultFrame );

    Eigen::MatrixXd jacobian( Eigen::MatrixXd::Zero(6,tree.getNrOfJoints()) );


//    ROS_INFO_STREAM( "Jacobian from " << baseSegName << " to " << tipSegName << " has " << kdlJacobian.columns() << " columns" );

    //Convert chain's jacobain to whole body jacobian
    direction = -1; // if the baseElement is pelvis then this should start as 1 instead

    lastChainSegName = baseSegName;
    for( int col = 0; col < kdlJacobian.columns(); ++col )
    {
        const KDL::Segment & chainSegment = chain.getSegment( col );
        const std::string &  chainSegName = chainSegment.getName();

        std::string treeSegName;

        if( direction == -1 )
            treeSegName = lastChainSegName;
        else
            treeSegName = chainSegName;

        const KDL::TreeElement & element = tree.getSegment( treeSegName )->second;

//        ROS_INFO_STREAM( "" );
//        ROS_INFO_STREAM( "direction           " << direction );
//        ROS_INFO_STREAM( "chainJoint number   " << col );
//        ROS_INFO_STREAM( "last chain segment  " << lastChainSegName );
//        ROS_INFO_STREAM( "chain segment       " << chainSegName );
//        ROS_INFO_STREAM( "tree segment        " << element.segment.getName() );
//        ROS_INFO_STREAM( "chain segment joint " << chainSegment.getJoint().getName() );
//        ROS_INFO_STREAM( "tree segment joint  " << element.segment.getJoint().getName() );
//        ROS_INFO_STREAM( "chain joint axis    " << Eigen::Vector3dConstMap( chainSegment.getJoint().JointAxis().data    ).transpose() );
//        ROS_INFO_STREAM( "tree joint axis     " << Eigen::Vector3dConstMap( element.segment.getJoint().JointAxis().data ).transpose() );

        int qIndex = element.q_nr;
        jacobian.col(qIndex) = kdlJacobian.data.col( col );

        std::string frameName;
        frameName = std::string("atlas_cmd/") + element.segment.getName();

        Eigen::Vector3d jointOffset(0,0,0);
//        if( direction == 1 )
//            jointOffset = Eigen::Vector3dMap( element.segment.getJoint().pose(0).p.data );

        if( vizDebugPublisher )
        {
//            ROS_INFO_STREAM( "Frame Name " << frameName << " offset is " << jointOffset.transpose() << " direction is " << direction );
            int hash = (int)BoostStringHash()( baseSegName + tipSegName );
            vizDebugPublisher->publishVectorFrom( jointOffset,
                                                  jacobian.col(qIndex).head<3>(),
                                                  colorFromHash(hash),
                                                  hash+col,
                                                  frameName, baseSegName + "_to_" + tipSegName + "_jacobian" );
        }

        if( tree.getSegment( chainSegName )->second.segment.getJoint().getType() == KDL::Joint::None ) // FIXME this needs to be verfied if it should be here or before the assignment
            direction = 1;

        BOOST_FOREACH( const KDL::SegmentMap::const_iterator & childItor, tree.getSegment( chainSegName )->second.children  )
        {
            if(    col + 1 < kdlJacobian.columns()
                && childItor->first == chain.getSegment( col + 1 ).getName() )
            {
                direction = 1;
            }
        }

        lastChainSegName = chainSegName;
    }

    return jacobian;
}


}
