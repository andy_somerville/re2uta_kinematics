/*
 * CenterOfMassController.cpp
 *
 *  Created on: Feb 28, 2013
 *      Author: andrew.somerville
 */

#include <re2uta/CenterOfMassController.hpp>
#include <re2uta/center_of_mass_jacobian.hpp>
#include <re2uta/calc_point_mass.hpp>
#include <re2uta/pinv.hpp>
#include <re2uta/tip_jacobian.hpp>

#include <kdl_parser/kdl_parser.hpp>
#include <kdl/tree.hpp>
#include <kdl/treefksolverpos_recursive.hpp>

#include <eigen_conversions/eigen_kdl.h>

#include <ros/ros.h>

#include <boost/shared_ptr.hpp>

#include <limits>


extern boost::shared_ptr<re2::VisualDebugPublisher> vizDebugPublisher;



namespace re2uta
{


CenterOfMassController::CenterOfMassController( const std::string & urdfString )
{
    kdl_parser::treeFromString( urdfString, m_tree );
    init();
}

CenterOfMassController::CenterOfMassController( const KDL::Tree & tree )
{
    m_tree = tree;
    init();
}

void CenterOfMassController::init()
{
    printTree( m_tree.getRootSegment()->second );
//            m_jointPositions.resize( m_tree.getNrOfJoints() );
//            ROS_INFO_STREAM( "Says we have " << m_tree.getNrOfJoints() << " joints" );
//
//            BOOST_FOREACH( KDL::SegmentMap::value_type const & entry, m_tree.getSegments() )
//            {
//                m_jointPositions( entry.second.q_nr ) = 0.0;
//            }


    m_treeFkSolver.reset( new KDL::TreeFkSolverPos_recursive( m_tree ) );
    // Build center of mass jacobian 3dof jabobian

    // Build left to right 6dof Jacobian
    // eliminate left to pelvis portions of that jacobian

    // combine jacobians

    KDL::SegmentMap::const_iterator item;
    item = m_tree.getRootSegment();
    if( item != m_tree.getSegments().end() )
        m_rootElement      = &item->second;
    else
        ROS_ASSERT_MSG( false, "Failed to find root" );

    item = m_tree.getSegment("l_foot" );
    if( item != m_tree.getSegments().end() )
        m_leftFootElement  = &item->second;
    else
        ROS_ASSERT_MSG( false, "Failed to find l_foot" );

    item = m_tree.getSegment("r_foot" );
    if( item != m_tree.getSegments().end() )
        m_rightFootElement = &item->second;
    else
        ROS_ASSERT_MSG( false, "Failed to find r_foot" );

    item = m_tree.getSegment("r_lglut");
    if( item != m_tree.getSegments().end() )
        m_rlglutElement    = &item->second;
    else
        ROS_ASSERT_MSG( false, "Failed to find r_lglut" );

    m_totalMass        = calcTreeMass(*m_rootElement);
}


Eigen::VectorXd
CenterOfMassController::calcVelocitiesForComCommand( const Eigen::VectorXd & jointPositions,
                                                     const Eigen::Vector3d & comDesiredVelocity,
                                                     const Eigen::Vector6d & rFootDesiredTwist   )
{
    Eigen::MatrixXd comJacobianFromLFoot;
    Eigen::MatrixXd tipJacobianRFootInLFoot;
    Eigen::MatrixXd balanceJacobian(  Eigen::MatrixXd::Zero(  6, m_tree.getNrOfJoints() ) );
    Eigen::VectorXd jointVelocityVector;

    Eigen::Affine3d rFootToLFootXForm = getTransform( "l_foot", "r_foot", jointPositions );

    comJacobianFromLFoot            = calcComJacobian( m_leftFootElement,                     jointPositions );
    tipJacobianRFootInLFoot         = calcTipJacobian( m_leftFootElement, m_rightFootElement, jointPositions, rFootToLFootXForm );

    m_lastComJacobianFromLFoot      = comJacobianFromLFoot;
    m_lastTipJacobianRFootInLFoot   = tipJacobianRFootInLFoot;


    int             numJoints             = m_tree.getNrOfJoints();

    int             goalDim               = comJacobianFromLFoot.rows() + tipJacobianRFootInLFoot.rows();
    Eigen::VectorXd goalVector( Eigen::VectorXd::Zero(goalDim) * std::numeric_limits<double>::quiet_NaN() );
    Eigen::MatrixXd masterJacobian        = Eigen::MatrixXd::Zero( goalDim, numJoints );


    goalVector.head<3>()                  = comDesiredVelocity;
    goalVector.tail<6>()                  = rFootDesiredTwist;

    masterJacobian.block(0,0,3,numJoints) = comJacobianFromLFoot;
    masterJacobian.block(3,0,6,numJoints) = tipJacobianRFootInLFoot;



    Eigen::MatrixXd invMasterJacobian     = pseudoInverse( masterJacobian );

    jointVelocityVector  = invMasterJacobian * goalVector;

//            commandJointVelocities( jointVelocityVector );
    return jointVelocityVector;
}


void
CenterOfMassController::commandJointVelocities( const Eigen::VectorXd & jointVelocityVector )
{
    ROS_INFO_STREAM( "Commanded velocities are:\n" << jointVelocityVector );
    assert(false);
}


Eigen::MatrixXd
CenterOfMassController::calcTipJacobian( const KDL::TreeElement * baseElement,
                                         const KDL::TreeElement * tipElement,
                                         const Eigen::VectorXd &  treeJointPositions,
                                         const Eigen::Affine3d &  defaultTransform )
{
    KDL::Chain chain;
    m_tree.getChain( baseElement->segment.getName(), tipElement->segment.getName(), chain );

    std::vector<bool> jointLockState( chain.getNrOfJoints(), false );
    int numUnlockedJoints = chain.getNrOfJoints();

    Eigen::VectorXd chainJointPositions( chain.getNrOfJoints() );
    for( int chainJoint = 0; chainJoint < chainJointPositions.rows(); ++chainJoint )
    {
        const KDL::TreeElement & element = m_tree.getSegment( chain.getSegment( chainJoint ).getName() )->second;
        chainJointPositions(chainJoint) = treeJointPositions( element.q_nr );
    }


    KDL::Jacobian kdlJacobian;
    kdlJacobian = re2uta::calculateTipJacobian( chain, chainJointPositions, jointLockState, numUnlockedJoints );

    Eigen::MatrixXd jacobian( Eigen::MatrixXd::Zero(6,m_tree.getNrOfJoints()) );

    //Convert chain's jacobain to whole body jacobian
    for( int col = 0; col < kdlJacobian.columns(); ++col )
    {
        const KDL::TreeElement & element = m_tree.getSegment( chain.getSegment( col ).getName() )->second;
        int qIndex = element.q_nr;
        jacobian.col(qIndex) = kdlJacobian.data.col( col );

        Eigen::Affine3d  jointPose          = getTransform( "l_foot", element.segment.getName(), treeJointPositions );

        if( vizDebugPublisher )
            vizDebugPublisher->publishVectorFrom( Eigen::Vector3d( jointPose.translation()                      ),
                                                  Eigen::Vector3d( jointPose.rotation() * jacobian.col(qIndex).head<3>() *0.1  ),
                                                  Eigen::Vector4d(0.9,0.5,0,1),
                                                  9000+qIndex, "l_foot" );
    }

    return jacobian;
}


Eigen::MatrixXd
CenterOfMassController::calcComJacobian( const KDL::TreeElement * treeElement,
                                         const Eigen::VectorXd &  jointPositions,
                                         const Eigen::Affine3d &  defaultTransform )
{
    Eigen::MatrixXd comJacobian( Eigen::MatrixXd::Ones(  3, m_tree.getNrOfJoints() ) * -1 );
    std::vector<bool>  visited(  m_tree.getNrOfJoints(), 0 );
    Eigen::Vector4d    pointMass(  0,0,0,0 );

    visited[ treeElement->q_nr ] = true;

    calcComJacBackward( defaultTransform, *treeElement,  jointPositions,  comJacobian,  pointMass, m_totalMass,  visited );

    return comJacobian;
}


Eigen::Vector4d
CenterOfMassController::calcComPointMass( const Eigen::VectorXd & jointPositions )
{
    std::vector<bool>  visited(  m_tree.getNrOfJoints(), 0 );
    Eigen::Vector4d    pointMass(  0,0,0,0 );

    calcPointMassBackward( Eigen::Affine3d::Identity(), *m_leftFootElement,  jointPositions,  pointMass, visited );

    return pointMass;
}


Eigen::Affine3d
CenterOfMassController::getTransform( const std::string & baseFrame, const std::string & tipFrame, const Eigen::VectorXd & jointPositions )
{
    KDL::Frame baseToPelvis;
    KDL::Frame tipToPelvis;


    KDL::JntArray kdlJointPositions;
    kdlJointPositions.data = jointPositions; //FIXME FSCK YOU KDL! What is the point of this damn class!

    int errorVal = 0;
    if( errorVal >= 0 )
        errorVal = m_treeFkSolver->JntToCart( kdlJointPositions,  baseToPelvis, baseFrame );
    if( errorVal != 0 )
    {
        ROS_WARN_STREAM( "jtnToCart for " << baseFrame << " to " << baseFrame << " gave error code: " << errorVal );
        ROS_WARN_STREAM( "Jointpositions were: " << jointPositions.transpose() );
        errorVal = 0;
    }

    if( errorVal >= 0 )
        errorVal = m_treeFkSolver->JntToCart( kdlJointPositions,   tipToPelvis, tipFrame );
    if( errorVal != 0 )
    {
        ROS_WARN_STREAM( "jtnToCart for " << baseFrame << " to " << tipFrame << " gave error code: " << errorVal );
        ROS_WARN_STREAM( "Jointpositions were: " << jointPositions.transpose() );
    }



    KDL::Frame tipToBase = baseToPelvis.Inverse() * tipToPelvis;

    Eigen::Affine3d tipToBaseTransform( Eigen::Affine3d::Identity() );
    tf::transformKDLToEigen( tipToBase, tipToBaseTransform );

    return tipToBaseTransform;
}


Eigen::Vector6d
CenterOfMassController::getPose( const std::string & baseFrame, const std::string & tipFrame, const Eigen::VectorXd & jointPositions )
{
    Eigen::Affine3d twistXform = getTransform( baseFrame, tipFrame, jointPositions );

    Eigen::Vector6d twist;
    twist.head<3>() = twistXform.translation();
    twist.tail<3>() = twistXform.rotation().eulerAngles(0,1,2); //FIXME could this be a problem because of singularities??

    return twist;
}




}
