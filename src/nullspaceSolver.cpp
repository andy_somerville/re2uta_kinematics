/*
 * nullspaceSolver.cpp
 *
 *  Created on: Apr 18, 2013
 *      Author: Isura Ranatunga
 */

#include <re2uta/FullBodyPoseSolver.hpp>
#include <re2uta/gradient_descent.hpp>
#include <re2uta/tip_jacobian.hpp>
#include <re2/kdltools/kdl_tree_util.hpp>
#include <re2uta/calc_point_mass.hpp>
#include <re2uta/center_of_mass_jacobian.hpp>
#include <re2uta/tip_jacobian.hpp>

#include <re2uta/AtlasLookup.hpp>

#include <re2uta/nullspaceSolver.hpp>

#include <re2uta/pinv.hpp>

namespace
{
    double nonNaN( const double & value )
    {
        if( std::isinf(value) || std::isnan(value) )
            return 0;
        else
            return value;
    }

        template< typename MatrixT0, typename MatrixT1>
        int appendRows( MatrixT0 & fullMatrix, const MatrixT1 & subMatrix, int & startRow )
        {
            fullMatrix.block(startRow,0,subMatrix.rows(),subMatrix.cols()) = subMatrix;
            startRow = startRow + subMatrix.rows();
            return startRow;
        }
}


namespace re2uta
{

/*
 * This function calculates the derivitive of the cost function (W) that is added to the deltaTheta
 * This follows the formulation in the 1977 paper Automatic Supervisory Control of the Configuration and Behavior of Multibody Mechanisms
 *                                          by Alain Liegeois for joint limits
 *              the formulation in the 1991 book Advanced Robotics: Redundancy and Optimization
 *                                          by Yoshihiko Nakamura
 *
 */
Eigen::VectorXd
NullSpaceFullBodySolver::delW( const Eigen::VectorXd & qMin,
                              const Eigen::VectorXd & qMax,
                              const Eigen::VectorXd & q )
{
        Eigen::VectorXd delW( q.rows() );

        if(   qMin.rows() == q.rows()
           && qMax.rows() == q.rows() )
        {
                  Eigen::VectorXd range = (qMax - qMin);

                  Eigen::VectorXd qMid = (qMax + qMin)/2;

                  // This is the derivative of the W function with respect to joints q
                  delW = (q - qMid).array() / (qMid - qMax).array().square();

//                ROS_INFO("Penalties Calculated!");

        }

        return delW.unaryExpr( std::ptr_fun(nonNaN) );
}

Eigen::VectorXd
NullSpaceFullBodySolver::solveForPoseNull( const KDL::Tree       & tree,
                                          const std::string     & baseElementName,
                                          const std::string     & tipElementName,
                                          const Eigen::VectorXd & q,
                                          const Eigen::Vector6d & desiredTipPose,
                                          const Eigen::Vector3d & desiredPostureOrientation,
                                          const Eigen::Vector3d & desiredComPose,
                                          const Eigen::VectorXd & qDes,
                                          const Eigen::VectorXd & qMin,
                                          const Eigen::VectorXd & qMax )
{

        re2uta::FullBodyPoseSolver::Ptr fullBodyPoseSolver;
        fullBodyPoseSolver.reset( new FullBodyPoseSolver( tree, qMin, qMax ) );

        // FIXME Change hard coded values
        Eigen::VectorXd tDes( 12, 1 );
        int atRow = 0;
        appendRows( tDes, desiredComPose,              atRow );
        appendRows( tDes, desiredPostureOrientation,   atRow );
        appendRows( tDes, desiredTipPose,              atRow );


        const KDL::TreeElement * baseElement;
        const KDL::TreeElement * postureElement;
        const KDL::TreeElement * tipElement;

        KDL::SegmentMap::const_iterator entry;
        entry = tree.getSegment( baseElementName );
        if( entry != tree.getSegments().end() )
                baseElement = &entry->second;
        else
                return Eigen::VectorXd();

        entry = tree.getSegment( "utorso" );
        //      entry = tree.getSegment( "pelvis" );
        if( entry != tree.getSegments().end() )
                postureElement = &entry->second;
        else
                return Eigen::VectorXd();

        entry = tree.getSegment( tipElementName );
        if( entry != tree.getSegments().end() )
                tipElement = &entry->second;
        else
                return Eigen::VectorXd();


        Eigen::Affine3d defaultTransform = Eigen::Affine3d::Identity();

        //ROS_ASSERT_MSG(false, "Sorry Isura, I changed the solver and broke this : /" );
        //ROS_ASSERT_MSG(false, "No problem Andy, I changed my solver and fixed it :)" );

        Eigen::MatrixXd J = Eigen::MatrixXd( fullBodyPoseSolver->calcMasterJacobian( q, defaultTransform ) );

        // TODO added weighted inverse

        // Jacobian pseudo inverse using damped least squares with lambda 0.1
        Eigen::MatrixXd Jinv = pseudoInverse( J, 0.1 );

        // Cartesian space error
        Eigen::VectorXd delT = tDes - fullBodyPoseSolver->calcCurrentPose( q, defaultTransform );

        // Here we calculate delta q the joints space difference to achieve the desired Cartesian space position
        Eigen::VectorXd delQ = Jinv*delT; // + (Eigen::MatrixXd::Identity(q.rows(),q.rows()) - Jinv * J)*delW(qMin, qMax, q);

        return delQ;

}



}



