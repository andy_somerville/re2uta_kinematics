/*
 * colors.cpp
 *
 *  Created on: Mar 21, 2013
 *      Author: andrew.somerville
 */


#include <re2uta/colors.hpp>



const Eigen::Array4d Red(    1,0,0,1 );
const Eigen::Array4d Green(  0,1,0,1 );
const Eigen::Array4d Blue(   0,0,1,1 );
const Eigen::Array4d Yellow( 1,1,0,1 );
const Eigen::Array4d Magenta(1,0,1,1 );
const Eigen::Array4d Cyan(   0,1,1,1 );
const Eigen::Array4d White(  1,1,1,1 );
const Eigen::Array4d Black(  0,0,0,1 );
const Eigen::Array4d DarkRed(    0.5,0,0,1 );
const Eigen::Array4d DarkGreen(  0,0.5,0,1 );
const Eigen::Array4d DarkBlue(   0,0,0.5,1 );
const Eigen::Array4d DarkYellow( 0.5,0.5,0,1 );
const Eigen::Array4d DarkMagenta(0.5,0,0.5,1 );
const Eigen::Array4d DarkCyan(   0,0.5,0.5,1 );
const Eigen::Array4d Gray(  0.5,0.5,0.5,1 );



