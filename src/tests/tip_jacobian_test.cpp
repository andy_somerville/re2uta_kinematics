/*
 * tip_jacobian_test.cpp
 *
 *  Created on: Jun 4, 2013
 *      Author: andrew.somerville
 */




/*
 * jacobian_test.cpp
 *
 *  Created on: Nov 21, 2012
 *      Author: somervil
 */


#include <re2uta/tip_jacobian.hpp>
#include <boost/foreach.hpp>
#include <re2/kdltools/kdl_tree_util.hpp>

#include <string>
#include <iostream>
#include <istream>
#include <ros/ros.h>


class TipJacobianTest
{
    public:
        void go()
        {
            using namespace re2uta;

            ros::NodeHandle node;

            std::string   simpleUrdfString;

            node.getParam( "/robot_description", simpleUrdfString );
            std::cout << "Robot_description: " << simpleUrdfString << std::endl;


            KDL::Tree tree;
            kdlRootInertiaWorkaround( simpleUrdfString, tree );



            const KDL::TreeElement * baseElement = &getTreeElement( tree, "r_hand" );
            const KDL::TreeElement * tip0        = &getTreeElement( tree, "l_hand" );
            const KDL::TreeElement * tip1        = &getTreeElement( tree, "r_foot" );

            Eigen::VectorXd treeJointPositions = Eigen::VectorXd::Ones(tree.getNrOfJoints());

            Eigen::MatrixXd jac0;
            Eigen::MatrixXd jac1;

            jac0 = calcTreeTipJacobian( tree,
                                        baseElement,
                                        tip0,
                                        treeJointPositions,
                                        Eigen::Affine3d::Identity() );

            jac1 = calcTreeTipJacobian( tree,
                                        baseElement,
                                        tip1,
                                        treeJointPositions,
                                        Eigen::Affine3d::Identity() );

            std::cout << "jac0 \n" << jac0.transpose() << "\n\n";
            std::cout << "jac1 \n" << jac1.transpose() << "\n\n";


//
//            while( ros::ok() )
//            {
//                ros::spinOnce();
//                for( int i = 0; i < jacobian1.columns(); ++i )
//                {
//                    debugPublisher.publishVectorFrom( Eigen::Vector3d(0,0,0), Eigen::Vector3d(jacobian1.data.col(i).head(3)), Eigen::Vector4d(0,0,1,1), 100+i );
//                }
//                debugPublisher.publishPoint3( com.head<3>(), Eigen::Vector4d(1,0,0,1), 3 );
//                ros::Duration(0.5).sleep();
//            }

        }
};


int main( int argc, char ** argv )
{
    ros::init( argc, argv, "tip_jacobian_test" );

    TipJacobianTest tipJacobianTest;

    tipJacobianTest.go();

    return 0;
}
