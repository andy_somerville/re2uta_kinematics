/*
 * pinv_test.cpp
 *
 *  Created on: Feb 5, 2013
 *      Author: andrew.somerville
 */

#include <re2uta/pinv.hpp>

#include <Eigen/Core>
#include <Eigen/Geometry>
#include <Eigen/SVD>

#include <iostream>

int main()
{
    typedef Eigen::Matrix<double,3,3> Mat3x3;
    typedef Eigen::Matrix<double,3,3> Mat3x3;

    Eigen::AngleAxis<double> transform0 = Eigen::AngleAxis<double>(  1.5, Eigen::Vector3d( 1,1,1 ).normalized() );
    Eigen::AngleAxis<double> transform1 = Eigen::AngleAxis<double>( -1.5, Eigen::Vector3d( 1,1,1 ).normalized() );


    Mat3x3          rotation0 = transform0.toRotationMatrix();
    Eigen::MatrixXd rotation1 = transform1.toRotationMatrix();

    Mat3x3          solution0 = pseudoInverse( rotation0 );
    Eigen::MatrixXd solution1 = pseudoInverse( rotation1 );
    Eigen::MatrixXd solution2 = weightedPseudoInverse( rotation1, Eigen::MatrixXd::Identity(3,3) * 0.01, Eigen::MatrixXd::Identity(3,3) );

    Eigen::MatrixXd diff0     = rotation1 - solution0 ;
    Mat3x3          diff1     = rotation0 - solution1 ;
    Eigen::MatrixXd diff2     = rotation0 - solution2 ;

    std::cout << "rot0:\n"  << rotation0 << "\n";
    std::cout << "rot1:\n"  << rotation1 << "\n";
    std::cout << std::endl;

    std::cout << "pinv0:\n" << solution0 << "\n";
    std::cout << "pinv1:\n" << solution1 << "\n";
    std::cout << "(iw)pinv2:\n" << solution2 << "\n";
    std::cout << std::endl;


    std::cout << "rot1 - pinv0:\n" << diff0 << "\n";
    std::cout << "rot0 - pinv1:\n" << diff1<< "\n";
    std::cout << "rot0 - pinv2:\n" << diff2<< "\n";
    std::cout << std::endl;


    double eps = 0.0001;
    std::cout << "eps: " << eps << "\n";
    std::cout << "(diff0.array() > eps).any(): " << (diff0.array() > eps).any() << "\n" ;
    std::cout << "(diff1.array() > eps).any(): " << (diff1.array() > eps).any() << "\n" ;
    std::cout << "(diff2.array() > eps).any(): " << (diff2.array() > eps).any() << "\n" ;
    std::cout << std::endl;

    typedef Eigen::Matrix<double,3,4> Mat3x4;
    typedef Eigen::Matrix<double,4,3> Mat4x3;

    Mat3x4 rect0;
    rect0.block<3,3>(0,0) = rotation0;
    rect0.block<3,1>(0,3) = Eigen::Vector3d( 1, 1, 1);

    Mat4x3 rect1;
    rect1.block<3,3>(0,0) = rotation1;
    rect1.block<1,3>(3,0) = Eigen::Vector3d(-1,-1,-1).transpose();

    Mat4x3 rectPinv0 = pseudoInverse( rect0 );
    Mat3x4 rectPinv1 = pseudoInverse( rect1 );
    Mat3x4 rectPinv2 = weightedPseudoInverse( rect1, Eigen::MatrixXd::Identity(3,3) * 0.01, Eigen::MatrixXd::Identity(4,4) );

    std::cout << "3x4 pinv sol:\n"     << rectPinv0 << "\n";
    std::cout << "4x3 pinv sol:\n"     << rectPinv1 << "\n";
    std::cout << "4x3 (iw)pinv sol:\n" << rectPinv2 << "\n";
    std::cout << std::endl;



    Eigen::MatrixXd directionJacobian(2,3);
    Eigen::MatrixXd centeringJacobian(3,3);
    Eigen::MatrixXd comboJacobian(5,3);
    Eigen::VectorXd directionGoalVector(2);
    Eigen::VectorXd centeringGoalVector(3); // goal is 100% of original vectors;
    Eigen::VectorXd comboGoalVector(5);
    centeringGoalVector << 1, 1, 1;
    directionGoalVector << 2, 1 ; // goal points at asin( 1/sqrt(3) );
    comboGoalVector.head(2) = directionGoalVector;
    comboGoalVector.tail(3) = centeringGoalVector;

    directionJacobian << 1, 0, 1,  // 3 vectors which point in x, y, and 45deg
                         0, 1, 1;

    centeringJacobian = Eigen::MatrixXd::Identity(3,3); // each element contributes to own direction perfectly

    comboJacobian = Eigen::MatrixXd::Zero( directionJacobian.rows() + centeringJacobian.rows(), centeringJacobian.cols() );
    comboJacobian.block(0,0,2,3) = directionJacobian;
    comboJacobian.block(2,0,3,3) = centeringJacobian;

    Eigen::MatrixXd outputWeightMatrix0 = Eigen::MatrixXd::Identity(5,5);
    Eigen::MatrixXd outputWeightMatrix1(5,5);
    Eigen::MatrixXd outputWeightMatrix2(5,5);
    outputWeightMatrix1 << 1,   0,   0,   0,   0,
                           0,   1,   0,   0,   0,
                           0,   0,   0.5, 0,   0,   // last centering matters half as much as direction
                           0,   0,   0,   0.5, 0,   // last centering matters half as much as direction
                           0,   0,   0,   0,   0.5; // last centering matters half as much as direction
    outputWeightMatrix2 << 1,   0,   0,   0,   0,
                           0,   1,   0,   0,   0,
                           0,   0,   0.001, 0,   0,   // last centering matters half as much as direction
                           0,   0,   0,   0.001, 0,   // last centering matters half as much as direction
                           0,   0,   0,   0,   0.001; // last centering matters half as much as direction


    Eigen::MatrixXd pinvComboJacobian0 = weightedPseudoInverse( comboJacobian, Eigen::MatrixXd::Identity(3,3), outputWeightMatrix0 );
    Eigen::MatrixXd pinvComboJacobian1 = weightedPseudoInverse( comboJacobian, Eigen::MatrixXd::Identity(3,3), outputWeightMatrix1 );
    Eigen::MatrixXd pinvComboJacobian2 = weightedPseudoInverse( comboJacobian, Eigen::MatrixXd::Identity(3,3), outputWeightMatrix2 );
    Eigen::VectorXd weightedSolution0  = pinvComboJacobian0 * comboGoalVector;
    Eigen::VectorXd weightedSolution1  = pinvComboJacobian1 * comboGoalVector;
    Eigen::VectorXd weightedSolution2  = pinvComboJacobian2 * comboGoalVector;
    Eigen::VectorXd weightedResult0    = comboJacobian      * weightedSolution0;
    Eigen::VectorXd weightedResult1    = comboJacobian      * weightedSolution1;
    Eigen::VectorXd weightedResult2    = comboJacobian      * weightedSolution2;

    std::cout << "comboJacobian jacobian:\n"                << comboJacobian   << "\n";
    std::cout << "weightedSolution0 output goal:\n"         << comboGoalVector   << "\n";
    std::cout << "weightedSolution0 solved input:\n"        << weightedSolution0 << "\n";
    std::cout << "weightedSolution1 solved input:\n"        << weightedSolution1 << "\n";
    std::cout << "weightedSolution1 solved input:\n"        << weightedSolution2 << "\n";
    std::cout << "weightedSolution0 output from solution:\n" << weightedResult0   << "\n";
    std::cout << "weightedSolution1 output from solution:\n" << weightedResult1   << "\n";
    std::cout << "weightedSolution2 output from solution:\n" << weightedResult2   << "\n";



    return 0;
}
