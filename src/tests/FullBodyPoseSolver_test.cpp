/*
 * FullBodyPoseSolverTest.cpp
 *
 *  Created on: Mar 14, 2013
 *      Author: andrew.somerville
 */


#include <re2uta/FullBodyPoseSolver.hpp>
#include <re2uta/ros_param_utils.hpp>
#include <re2uta/colors.hpp>
#include <re2/kdltools/kdl_tree_util.hpp>

#include <robot_state_publisher/robot_state_publisher.h>
#include <re2/visutils/VisualDebugPublisher.h>
#include <re2uta/AtlasDebugPublisher.hpp>

#include <tf/transform_broadcaster.h>

#include <re2/kdltools/kdl_tools.h>

#include <kdl_parser/kdl_parser.hpp>
#include <urdf_model/model.h>

#include <ros/ros.h>

#include <iostream>
#include <fstream>


double nonNaN( const double & value )
{
    if( std::isinf(value) || std::isnan(value) )
        return 0;
    else
        return value;
}


boost::shared_ptr<re2::VisualDebugPublisher>   vizDebugPublisher;
boost::shared_ptr<re2uta::AtlasDebugPublisher> atlasDebugPublisher;

using namespace re2uta;

class FullBodyPoseSolverTest2
{
    private:
        typedef std::map<std::string, double> JointPositionMap;
        typedef std::map<std::string,double> StringDoubleMap;


        ros::NodeHandle  m_node;
        KDL::Tree        m_tree;
        urdf::Model      m_urdfModel;
        Eigen::VectorXd  m_jointPositions;
        Eigen::VectorXd  m_jointMins;
        Eigen::VectorXd  m_jointMaxs;
        JointPositionMap m_jointPositionMap;

        double           m_comZ;
        double           m_comY;
        double           m_legBend;
        double           m_footX;
        double           m_footZ;

        tf::TransformBroadcaster                     m_broadcaster;


    public:
        FullBodyPoseSolverTest2()
        {
            std::string urdfFileName = "simple_arm.urdf" ;
            //m_node.getParam( "urdf_file_name", urdfFileName );

            m_comZ = 0.93;
            m_node.getParam( "com_z", m_comZ );

            m_comY = -0.084;
            m_node.getParam( "com_y", m_comY );

            m_legBend = 0.4;
            m_node.getParam( "leg_bend", m_legBend );

            m_footX = 0.24;
            m_node.getParam( "foot_x", m_footX );

            m_footZ = 0.05;
            m_node.getParam( "foot_z", m_footZ );

            //std::ifstream simpleUrdfFile( urdfFileName.c_str() );
            std::string   simpleUrdfString; //(  (std::istreambuf_iterator<char>(simpleUrdfFile)),
                                            // (std::istreambuf_iterator<char>()              )  );


            m_node.getParam( "/robot_description", simpleUrdfString );

            m_urdfModel.initString( simpleUrdfString );

            re2::kdltools::JointNameToAngleMap jointMins;
            re2::kdltools::JointNameToAngleMap jointMaxs;

            re2::kdltools::getJointLimits( m_urdfModel, jointMins, jointMaxs );


            kdlRootInertiaWorkaround( m_urdfModel, m_tree );
            printTree( m_tree );

            m_jointPositions = Eigen::VectorXd::Ones( m_tree.getNrOfJoints() ) *   std::numeric_limits<double>::quiet_NaN();
            m_jointMins      = Eigen::VectorXd::Ones( m_tree.getNrOfJoints() ) * - std::numeric_limits<double>::infinity();
            m_jointMaxs      = Eigen::VectorXd::Ones( m_tree.getNrOfJoints() ) *   std::numeric_limits<double>::infinity();

            ROS_INFO_STREAM( "Says we have " << m_tree.getNrOfJoints() << " joints" );

            int index = 0;
            BOOST_FOREACH( KDL::SegmentMap::value_type const & entry, m_tree.getSegments() )
            {
                KDL::Segment const & segment = entry.second.segment;
                m_jointPositionMap[ segment.getJoint().getName() ] = 0.0;
                m_jointPositions( entry.second.q_nr ) = 0.0;

                ROS_INFO_STREAM( "segmentName: " << segment.getName() << " jointName: " << segment.getJoint().getName() << " Qnr: " << entry.second.q_nr );

                re2::kdltools::JointNameToAngleMap::iterator minEntry;
                minEntry = jointMins.find( segment.getJoint().getName() );
                if( minEntry != jointMins.end() )
                {
                    m_jointMins( entry.second.q_nr ) = minEntry->second;
                    ROS_INFO_STREAM( "min: " << minEntry->second );
                }

                re2::kdltools::JointNameToAngleMap::iterator maxEntry;
                maxEntry = jointMaxs.find( segment.getJoint().getName() );
                if( maxEntry != jointMaxs.end() )
                {
                    m_jointMaxs( entry.second.q_nr ) = maxEntry->second;
                    ROS_INFO_STREAM( "max: " << maxEntry->second );
                }

                ++index;
            }

        }

        void go()
        {
            robot_state_publisher::RobotStatePublisher robotStatePublisher( m_tree );

            std::string baseSegmentName(    "left_foot" );
            std::string oppFootSegmentName( "right_foot" );
            std::string postureSegmentName( "upper_torso" );
            std::string leftHand(           "left_hand" );
            std::string rightHand(          "right_hand" );

            m_node.getParam( "base_seg",       baseSegmentName    );
            m_node.getParam( "opposite_seg",   oppFootSegmentName );
            m_node.getParam( "posture_seg",    postureSegmentName );
            m_node.getParam( "left_hand_seg",  leftHand  );
            m_node.getParam( "right_hand_seg",  rightHand );

            std::string baseFoot( baseSegmentName    );
            std::string moveFoot( oppFootSegmentName );

            vizDebugPublisher.reset(   new re2::VisualDebugPublisher( "com_position_markers", baseSegmentName ) );
            atlasDebugPublisher.reset( new re2uta::AtlasDebugPublisher(m_urdfModel) );

            atlasDebugPublisher->setBase(baseFoot);
            vizDebugPublisher->setDefaultFrame(baseFoot);


            FullBodyPoseSolver poseSolver(m_urdfModel,
                                         baseSegmentName,
                                         oppFootSegmentName,
                                         postureSegmentName );


            const KDL::TreeElement * baseElement    = &(m_tree.getSegment( baseFoot )->second);
            const KDL::TreeElement * oppFootElement = &(m_tree.getSegment( moveFoot )->second);

            poseSolver.oppFootConstraint()->setBaseElement( baseFoot );
            poseSolver.oppFootConstraint()->setTipElement(  moveFoot );
            poseSolver.comConstraint()->    setBaseElement( baseElement ); // FIXME this should be handled in a loop, but requires increased interface complexity

            FrameConstraint::Ptr leftHandConstraint; // = poseSolver.addFrameConstraint( baseFoot, leftHand );
            FrameConstraint::Ptr rightHandConstraint;// = poseSolver.addFrameConstraint( leftHand, rightHand );

            double sign =  -1;
            poseSolver.comConstraint()->setGoal( Eigen::Vector3d( 0, sign * m_comY, m_comZ ) );
            poseSolver.comConstraint()->goalWeightVector()(2)        = 0.0001;

//            leftHandConstraint->goalWeightVector()                  << 0.0,0.0,0.0,0.5,0.5,0.5;
//            rightHandConstraint->goalWeightVector()                 << 0.5,0.5,0.5,0.5,0.5,0.5;

            poseSolver.postureConstraint()->setGoal( Eigen::Vector6d() );
            poseSolver.postureConstraint()->goal()                  << 0, 0, 0, 0,      0,     0;
            poseSolver.postureConstraint()->goalWeightVector()      << 0, 0, 0, 0.0001, 0.001, 0.0001;

            poseSolver.optimalAngleConstraint()->goal()             = ((m_jointMaxs + m_jointMins)/2).unaryExpr( std::ptr_fun( nonNaN ) );
            poseSolver.optimalAngleConstraint()->goalWeightVector() = Eigen::VectorXd::Ones(poseSolver.optimalAngleConstraint()->goal().size()) * 0.01;


            typedef std::map<std::string,SolverConstraint::Ptr> StringConstraintMap;
            StringConstraintMap constraintMap;

            constraintMap[leftHand       ] = leftHandConstraint;
            constraintMap[rightHand      ] = rightHandConstraint;
            constraintMap["optimal_angle"] = poseSolver.optimalAngleConstraint();
            constraintMap["posture"      ] = poseSolver.postureConstraint();
            constraintMap["com"          ] = poseSolver.comConstraint();

            BOOST_FOREACH( StringConstraintMap::value_type & entry, constraintMap )
            {
                int index = 0;
                std::string param_key;
                const std::string &     constraintName = entry.first;
                SolverConstraint::Ptr & constraint     = entry.second;

                if( constraint == NULL )
                {
                    std::string constraint_frame;
                    if( m_node.getParam( "frame", constraint_frame ) )
                        constraint = poseSolver.addFrameConstraint( constraint_frame, constraintName );
                    else
                        constraint = poseSolver.addFrameConstraint( baseFoot, constraintName );
                }


                index = 0;
                param_key = std::string( "/constraints/" ) + entry.first + std::string( "/goal_weight/" );
                traverseParameterVector<double>( m_node, param_key,
                    [&] (double entry){
                        constraint->goalWeightVector()(index) = entry;
                        ROS_INFO( "%s [%i] : %f", param_key.c_str(), index, entry );
                        ++index;
                    }
                );

                index = 0;
                param_key = std::string( "/constraints/" ) + entry.first + std::string( "/goal/" );
                traverseParameterVector<double>( m_node, param_key,
                    [&] (double entry){
                        constraint->goal()(index) = entry;
                        ROS_INFO( "%s [%i] : %f", param_key.c_str(), index, entry );
                        ++index;
                    }
                );

                param_key = std::string( "/constraints/" ) + entry.first + std::string( "/goal_map/" );
                traverseParameterMap<double>( m_node, param_key,
                    [&] (const StringDoubleMap::value_type & entry){
                        const std::string & key   = entry.first;
                        const double &      value = entry.second;
                        constraint->goal()( m_tree.getSegment( key  )->second.q_nr ) =  value;
                        ROS_INFO( "%s %s : %f", param_key.c_str(), key.c_str(), value );
                    }
                );
            }

            leftHandConstraint  = boost::dynamic_pointer_cast<FrameConstraint>(constraintMap[leftHand]);
            rightHandConstraint = boost::dynamic_pointer_cast<FrameConstraint>(constraintMap[rightHand]);


            m_jointPositions = poseSolver.optimalAngleConstraint()->goal();

            Eigen::Vector6d originalOppFootPose   = poseSolver.getPoseTwist( baseFoot, moveFoot, m_jointPositions );
            Eigen::Vector6d originalLeftHandPose  = poseSolver.getPoseTwist( baseFoot, leftHand, m_jointPositions );
            Eigen::Vector6d originalRightHandPose = poseSolver.getPoseTwist( leftHand, rightHand, m_jointPositions );
            ROS_WARN_STREAM( "originalOppFootPose:   " << originalOppFootPose.transpose()   );
            ROS_WARN_STREAM( "originalRightHandPose: " << originalRightHandPose.transpose() );


            Eigen::Vector6d desiredOppFootPose   = originalOppFootPose;
            int counter =   0;
            int parts   = 100;
            while( ros::ok() )
            {
                //leftHandConstraint->setBaseElement( baseElement );

                desiredOppFootPose.x() = /*std::max( 0.0,*/ std::sin( counter/(double)parts    * M_PI )*m_footX /*)*/;
                desiredOppFootPose.z() =   std::max( 0.0,   std::cos( counter/(double)parts    * M_PI )*m_footZ );

                poseSolver.oppFootConstraint()->setGoal( desiredOppFootPose );
                leftHandConstraint->setGoal(  originalLeftHandPose  );
                rightHandConstraint->setGoal( originalRightHandPose );

                ROS_INFO_STREAM( "about to solve" );
                Eigen::VectorXd currentPose = poseSolver.calcCurrentPose( m_jointPositions, Eigen::Affine3d::Identity() );
                Eigen::VectorXd jointPose   = poseSolver.solvePose( m_jointPositions );

                vizDebugPublisher->publishPoint3(poseSolver.comConstraint()->goal(),Red,99999999);

                if( jointPose.rows() == 0 )
                {
                    ROS_ERROR( "empty solution" );
                    jointPose = m_jointPositions;
                }

                BOOST_FOREACH( KDL::SegmentMap::value_type const & entry, m_tree.getSegments() )
                {
                    const KDL::Segment  & segment      = entry.second.segment;

                    if( segment.getJoint().getType() != KDL::Joint::None )
                    {
                        int jointId = entry.second.q_nr;
                        Eigen::Vector3d rotationAxis;
                        rotationAxis = Eigen::Map<Eigen::Vector3d>( segment.getJoint().JointAxis().data );

                        m_jointPositions(jointId)                           = jointPose(jointId);
                        m_jointPositionMap[ segment.getJoint().getName() ]  = m_jointPositions(jointId);
                    }

                }

                ros::Duration(0.002).sleep();
                robotStatePublisher.publishFixedTransforms();
                robotStatePublisher.publishTransforms( m_jointPositionMap, ros::Time::now() );

                ++counter;
            }
        }

    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW

};


int main( int argc, char ** argv )
{
    ros::init( argc, argv, "re2uta_kinematics_fullbodyposesolver_test2" );
    ros::NodeHandle node;

    FullBodyPoseSolverTest2 fullBodyPoseSolverTest2;

    fullBodyPoseSolverTest2.go();

    return 0;
}
