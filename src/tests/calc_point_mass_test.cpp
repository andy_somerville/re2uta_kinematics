/*
 * calc_point_mass_test.cpp
 *
 *  Created on: Apr 10, 2013
 *      Author: andrew.somerville
 */


#include <re2uta/calc_point_mass.hpp>
#include <re2/kdltools/kdl_tree_util.hpp>

#include <re2/kdltools/kdl_tools.h>
#include <ros/ros.h>

#include <kdl/segment.hpp>
#include <kdl/joint.hpp>
#include <kdl_parser/kdl_parser.hpp>
#include <Eigen/Core>
#include <Eigen/Geometry>

#include <boost/shared_ptr.hpp>
#include <boost/foreach.hpp>

#include <iostream>
#include <fstream>

class CalcPointMassTest
{
    private:
        ros::NodeHandle  m_node;
        KDL::Tree        m_tree;

    public:
        CalcPointMassTest()
        {
            std::string urdfString;

            m_node.getParam( "/robot_description", urdfString );

            urdf::Model urdfModel;
            urdfModel.initString( urdfString );
            kdlRootInertiaWorkaround( urdfString, m_tree );

            printTree( m_tree );
        }


        void go()
        {
            Eigen::VectorXd jointAngles;

            jointAngles = Eigen::VectorXd::Zero(m_tree.getNrOfJoints());

            double treeMass = calcTreeMass( m_tree );
            ROS_INFO_STREAM( "calcTreeMass says: " << treeMass );


            Eigen::Vector4d pointMassL = calcPointMassOld( m_tree, m_tree.getSegment("left_foot")->second, jointAngles );
            ROS_INFO_STREAM( "calcPointMassOld l_foot" << pointMassL.transpose() );

            Eigen::Vector4d pointMassR = calcPointMassOld( m_tree, m_tree.getSegment("right_foot")->second, jointAngles );
            ROS_INFO_STREAM( "calcPointMassOld r_foot " << pointMassR.transpose() );

            Eigen::Vector4d pointMassL0 = calcPointMass0( m_tree, m_tree.getSegment("left_foot")->second, jointAngles );
            ROS_INFO_STREAM( "calcPointMass0 l_foot " << pointMassL0.transpose() );

            Eigen::Vector4d pointMassR0 = calcPointMass0( m_tree, m_tree.getSegment("right_foot")->second, jointAngles );
            ROS_INFO_STREAM( "calcPointMass0 r_foot " << pointMassR0.transpose() );

            Eigen::Vector4d pointMassL1 = calcPointMass1( m_tree, m_tree.getSegment("left_foot")->second, jointAngles );
            ROS_INFO_STREAM( "calcPointMass1 l_foot " << pointMassL1.transpose() );

            Eigen::Vector4d pointMassR1 = calcPointMass1( m_tree, m_tree.getSegment("right_foot")->second, jointAngles );
            ROS_INFO_STREAM( "calcPointMass1 r_foot " << pointMassR1.transpose() );


            double lVsRDiff        = pointMassL(3) - pointMassR(3);
            double lVsTreeDiffOld  = pointMassL(3) - treeMass     ;
            double lVsTreeDiff0    = pointMassL0(3) - treeMass     ;
            double lVsTreeDiff1    = pointMassL1(3) - treeMass     ;

            ROS_INFO_STREAM( "Mass diff between L  and R is:    " << lVsRDiff     );

            ROS_INFO_STREAM( "Mass diff between LOld  and Tree is: " << lVsTreeDiffOld  );
            if( std::abs( lVsRDiff ) > 0.1 )
                ROS_ERROR_STREAM( "L vs R diff is high!" );

            ROS_INFO_STREAM( "Mass diff between L0 and Tree is: " << lVsTreeDiff0 );
            if( std::abs( lVsTreeDiff0 ) > 0.1 )
                ROS_ERROR_STREAM( "L vs Tree diff is high!" );

            ROS_INFO_STREAM( "Mass diff between L1 and Tree is: " << lVsTreeDiff1 );
            if( std::abs( lVsTreeDiff1 ) > 0.1 )
                ROS_ERROR_STREAM( "L vs Tree diff is high!" );





            Eigen::Vector4d pm0;
            Eigen::Vector4d pm1;
            Eigen::Vector4d resultPm;

            pm0 = Eigen::Vector4d( 1,0,0,1);
            pm1 = Eigen::Vector4d(-1,0,0,0.5);
            resultPm = aggrigatePointMasses( pm0, pm1 );
            ROS_INFO_STREAM( "pm0 " << pm0.transpose() << " + pm1 " << pm1.transpose() << " = " << resultPm.transpose() );

            pm0 = Eigen::Vector4d( 1,0,0,1);
            pm1 = Eigen::Vector4d(-1,0,0,1);
            resultPm = aggrigatePointMasses( pm0, pm1 );
            ROS_INFO_STREAM( "pm0 " << pm0.transpose() << " + pm1 " << pm1.transpose() << " = " << resultPm.transpose() );
        }
};


int main( int argc, char ** argv )
{
    ros::init( argc, argv, "AtlasMetrics" );

    CalcPointMassTest calcPointMassTest;

    calcPointMassTest.go();

    return 0;
}

