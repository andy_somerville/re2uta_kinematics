/*
 * zmp_concept_test.cpp
 *
 *  Created on: Jan 18, 2013
 *      Author: andrew.somerville
 */



#include <re2/kdltools/kdl_tools.h>
#include <visualization_msgs/Marker.h>

#include <re2/visutils/VisualDebugPublisher.h>

#include <Eigen/Core>
#include <Eigen/Geometry>

#include <boost/shared_ptr.hpp>
#include <boost/foreach.hpp>

#include <iostream>
#include <fstream>


class World
{
    public:
        re2::VisualDebugPublisher visDebug;
        const Eigen::Vector4d     Red;
        const Eigen::Vector4d     Green;
        const Eigen::Vector4d     Blue;
        const Eigen::Vector4d     Yellow;
        const Eigen::Vector4d     Teal;
        const Eigen::Vector4d     Purple;
        const Eigen::Vector4d     White;
        const Eigen::Vector4d     Black;

        World()
            :visDebug( "zmp_debug" ),
             Red(    1, 0, 0, 1 ),
             Green(  0, 1, 0, 1 ),
             Blue(   0, 0, 1, 1 ),
             Yellow( 1, 1, 0, 1 ),
             Teal(   0, 1, 1, 1 ),
             Purple( 1, 0, 1, 1 ),
             White(  1, 1, 1, 1 ),
             Black(  0, 0, 0, 1 )
        {}


        Eigen::Vector3d
        calcZeroMomentPoint( const Eigen::Vector3d     & originPointM,
                             const Eigen::Vector3d     & sensorPointM,
                             const Eigen::Hyperplane3d & footPlaneM,
                             const Eigen::Vector3d     & sensedForceN,
                             const Eigen::Vector3d     & sensedTorqueNM )
        {
            Eigen::Vector3d           momentArmDir;
            double                    momentArmLenM;
            Eigen::Vector3d           netForcePointM;
            Eigen::ParametrizedLine3d zeroMomentLine;
            Eigen::Vector3d           zeroMomentPointM;


            momentArmDir     = sensedForceN.cross( sensedTorqueNM ).normalized();
            momentArmLenM    = sensedTorqueNM.norm() / sensedForceN.norm();
            netForcePointM   = sensorPointM + momentArmDir * momentArmLenM;
            zeroMomentLine   = Eigen::ParametrizedLine3d( netForcePointM, sensedForceN.normalized() );
            zeroMomentPointM = re2::intersectLineWithPlane( zeroMomentLine, footPlaneM );


            visDebug.publishSegment(    originPointM,   sensorPointM,     Red,    1 );
            visDebug.publishSegment(    sensorPointM,   netForcePointM,   Yellow, 2 );
            visDebug.publishVectorFrom( sensorPointM,   sensedForceN,     Teal,   3 );
            visDebug.publishVectorFrom( sensorPointM,   sensedTorqueNM,   Purple, 4 );
            visDebug.publishSegment(    sensorPointM,   netForcePointM,   Yellow, 5 );
            visDebug.publishLine(       zeroMomentLine, 4,                Green,  6 );
//            visDebug.publishSegment(    netForcePointM, zeroMomentPointM, Green,  6 );
            visDebug.publishVectorFrom( netForcePointM, sensedForceN,     Teal,   7 );
            visDebug.publishSegment(    originPointM,   zeroMomentPointM, Red,    8 );

            return zeroMomentPointM;
        }
};


int main( int argc, char ** argv )
{
    ros::init( argc, argv, "zmp_concept_test" );

    World world;

    // Given
    Eigen::Vector3d     originPointM(   0.0, 0.0,  0.0 );
    Eigen::Vector3d     sensorPointM(   0.0, 0.0,  0.5 );
    Eigen::Hyperplane3d footPlaneM( Eigen::ZAxis3d, originPointM );
    Eigen::Vector3d     sensedForceN(   0.0,-0.5, -0.5 );
    Eigen::Vector3d     sensedTorqueNM( 1.0, 0.0,  0.0 );


    Eigen::Vector3d     zeroMomentPoint;


    while( ros::ok() )
    {
        zeroMomentPoint = world.calcZeroMomentPoint( originPointM, sensorPointM, footPlaneM, sensedForceN, sensedTorqueNM );
        std::cout << "ZeroMomentPoint : " << zeroMomentPoint.transpose() << std::endl;
        ros::spinOnce();
        ros::Duration(1).sleep();
    }

    return 0;
}

