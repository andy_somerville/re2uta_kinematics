/*
 * CenterOfMassVelocitySolver.cpp
 *
 *  Created on: Feb 7, 2013
 *      Author: andrew.somerville
 */


#include <re2uta/debug.hpp>
#include <re2uta/center_of_mass_jacobian.hpp>

#include <kdl/tree.hpp>
#include <boost/static_assert.hpp>
#include <boost/foreach.hpp>
#include <limits>

#include <Eigen/Core>
#include <kdl/chain.hpp>
#include <kdl/tree.hpp>

#include <ros/ros.h>


#include <re2/visutils/VisualDebugPublisher.h>

namespace //create anonymous namespace to avoid link collisions
{

static const double NaNd = std::numeric_limits<double>::quiet_NaN();


void calcComJac(      const Eigen::Affine3d &  lastToBaseFrame,
                      const KDL::TreeElement & element,
                      const Eigen::VectorXd &  jointAngles,
                      Eigen::MatrixXd &        jacobian_out,
                      Eigen::Vector4d &        postPointMass_out,
                      const double             totalMass,
                      std::vector<bool> &      visited_out );

void calcComJacBackward( const Eigen::Affine3d &  lastToBaseFrame,
                      const KDL::TreeElement & prevElement,
                      const Eigen::VectorXd &  jointAngles,
                      Eigen::MatrixXd &        jacobian_out,
                      Eigen::Vector4d &        postPointMass_out,
                      const double             totalMass,
                      std::vector<bool> &      visited_out );

void calcComJac_internal( const Eigen::Affine3d &  toLowerFrame,
                          const Eigen::Affine3d &  toBaseFrame,
                          const Eigen::Vector3d &  jointLocation,
                          const KdlElementChildren & children,
                          const KDL::TreeElement & element,
                          const Eigen::VectorXd &  jointAngles,
                          Eigen::MatrixXd &        jacobian_out,
                          const Eigen::Vector4d &  localPointMass,
                          Eigen::Vector4d &        postPointMass_out,
                          const Eigen::Vector3d &  jointAxis,
                          const int                jointIndex,
                          const std::string &      frameName,
                          const double             totalMass,
                          std::vector<bool> &      visited_out )
{
    BOOST_FOREACH( const KDL::SegmentMap::const_iterator & itor, children )
    {
        const KDL::TreeElement & childElement = itor->second;

        if(    childElement.segment.getJoint().getType() == KDL::Joint::None  // FIXME this hack to get around KDL stupidity will likely cause an infinite loop eventually
            || visited_out[ childElement.q_nr ] )
        {
            continue;
        }

        Eigen::Vector4d childPostPointMass(0,0,0,0);
        calcComJac( toBaseFrame, childElement, jointAngles, jacobian_out, childPostPointMass, totalMass, visited_out );
        postPointMass_out = aggrigatePointMasses( childPostPointMass, postPointMass_out );
    }


    if(    element.segment.getJoint().getType() != KDL::Joint::None
        && !visited_out[ element.parent->second.q_nr]                 )
    {
        Eigen::Vector4d childPostPointMass(0,0,0,0);
        calcComJacBackward( toBaseFrame, element, jointAngles, jacobian_out, childPostPointMass, totalMass, visited_out );
        postPointMass_out = aggrigatePointMasses( childPostPointMass, postPointMass_out );
    }


    Eigen::Vector3d jointAxisInBase;
    Eigen::Vector3d centerOfMassInLocal;
    Eigen::Vector3d velocityInLocal;
    Eigen::Vector3d scaledVelocityInLocal;
    Eigen::Vector3d scaledVelocityInBase;
    double          postMassProportion;



    postPointMass_out     = aggrigatePointMasses( postPointMass_out, localPointMass        );
    centerOfMassInLocal   = postPointMass_out.head<3>();
//    velocityInBase        = jointAxisInBase.cross( centerOfMassInBase ); // Instantaneous velocity of target due to rotational velocity of joint
    velocityInLocal       = jointAxis.cross( centerOfMassInLocal ); // Instantaneous velocity of target due to rotational velocity of joint
    postMassProportion    = (postPointMass_out(3)/totalMass);
    scaledVelocityInLocal = velocityInLocal * postMassProportion;
    scaledVelocityInBase  = toBaseFrame.rotation() * scaledVelocityInLocal;


    if( vizDebugPublisher )
    {
        vizDebugPublisher->publishVectorFrom( Eigen::Vector3d( 0, 0, 0 ) + jointLocation,
                                              Eigen::Vector3d( scaledVelocityInLocal ),
                                              Eigen::Vector4d(0.5,0,0,1),
                                              BoostStringHash()( frameName + "tip_jacobian" )+jointIndex,
                                              std::string("atlas_cmd/") + frameName, "com_jacobian" );
    }


    assert( jacobian_out.cols() > jointIndex );

    jacobian_out.col( jointIndex ) = scaledVelocityInBase;
    postPointMass_out.head<3>()    = toLowerFrame * postPointMass_out.head<3>();

    return;
}

void calcComJacBackward( const Eigen::Affine3d &  lastToBaseFrame,
                         const KDL::TreeElement & prevElement,
                         const Eigen::VectorXd &  jointAngles,
                         Eigen::MatrixXd &        jacobian_out,
                         Eigen::Vector4d &        postPointMass_out,
                         const double             totalMass,
                         std::vector<bool> &      visited_out )

{
    const KDL::TreeElement & element      = prevElement.parent->second;
    const KDL::Segment &     prevSegment  = prevElement.segment;
    const KDL::Segment &     segment      = element.segment;
    const KDL::Joint   &     joint        = prevSegment.getJoint();
    const KDL::Joint   &     nextJoint    = segment.getJoint();
    Eigen::Vector3d          jointAxis    = Eigen::Map< Eigen::Vector3d >( joint.JointAxis().data) ;
    int                      jointIndex   = prevElement.q_nr;


    if( joint.getType() != KDL::Joint::None  )
    {
        visited_out[ jointIndex ] = true;
    }

    Eigen::Vector3d jointOffset = Eigen::Vector3dMap( joint.pose(0).p.data );
    Eigen::Affine3d toBaseFrame = lastToBaseFrame;
    Eigen::Affine3d translation;
    Eigen::Affine3d rotation;
    Eigen::Affine3d toLowerFrame;

    translation   = Eigen::Translation3d( -jointOffset );
    rotation      = Eigen::AngleAxisd( -jointAngles( jointIndex ), jointAxis );
    toLowerFrame  = rotation * translation;
    toBaseFrame   = (toBaseFrame * toLowerFrame);

    Eigen::Vector4d localPointMass;
    localPointMass.head<3>()    = Eigen::Vector3dMap( segment.getInertia().getCOG().data );
    localPointMass.tail<1>()(0) = segment.getInertia().getMass();


    calcComJac_internal( toLowerFrame,
                         toBaseFrame,
                         jointOffset,
                         element.children,
                         element,
                         jointAngles,
                         jacobian_out,
                         localPointMass,
                         postPointMass_out,
                        -jointAxis,
                         jointIndex,
                         segment.getName(),
                         totalMass,
                         visited_out );

    return;
}


void calcComJac( const Eigen::Affine3d &  lastToBaseFrame,
                 const KDL::TreeElement & element,
                 const Eigen::VectorXd &  jointAngles,
                 Eigen::MatrixXd &        jacobian_out,
                 Eigen::Vector4d &        postPointMass_out,
                 const double             totalMass,
                 std::vector<bool> &      visited_out )
{
    const KDL::Segment &  segment    = element.segment;
    const KDL::Joint   &  joint      = segment.getJoint();
    Eigen::Vector3d       jointAxis  = Eigen::Map< Eigen::Vector3d >( joint.JointAxis().data) ;
    int                   jointIndex = element.q_nr;

    if( joint.getType()  != KDL::Joint::None )
        visited_out[ jointIndex ] = true;

    Eigen::Vector3d jointOffset = Eigen::Vector3dMap( joint.pose(jointAngles(jointIndex)).p.data ); //FIXME warning this may not match my concept for what joint offset is

    Eigen::Affine3d toBaseFrame = lastToBaseFrame;
    Eigen::Affine3d toLowerFrame;
    toLowerFrame = Eigen::Translation3d( jointOffset );
    toLowerFrame = toLowerFrame * Eigen::AngleAxisd( jointAngles( jointIndex ), jointAxis ); // note post-multiply means reverse rotation

    toBaseFrame  = toBaseFrame * toLowerFrame;


    Eigen::Vector4d localPointMass;
    localPointMass.head<3>()    = Eigen::Vector3dMap( segment.getInertia().getCOG().data );
    localPointMass.tail<1>()(0) = segment.getInertia().getMass();


    calcComJac_internal( toLowerFrame,
                      toBaseFrame,
                      Eigen::Zero3d,
                      element.children,
                      element,
                      jointAngles,
                      jacobian_out,
                      localPointMass,
                      postPointMass_out,
                      jointAxis,
                      jointIndex,
                      segment.getName(),
                      totalMass,
                      visited_out );
}

}// end anonymous namespace


Eigen::MatrixXd
calcTreeComJacobian( const KDL::Tree &        tree,
                     const KDL::TreeElement * treeElement,
                     const Eigen::VectorXd &  jointPositions,
                     const Eigen::Affine3d &  defaultTransform,
                     double                   totalMass )
{
    Eigen::MatrixXd comJacobian( Eigen::MatrixXd::Ones(  3, tree.getNrOfJoints() ) * -1 );
    std::vector<bool>  visited(  tree.getNrOfJoints(), 0 );
    Eigen::Vector4d    pointMass(  0,0,0,0 );

    visited[ treeElement->q_nr ] = true;


    calcComJacBackward( defaultTransform, *treeElement,  jointPositions,  comJacobian,  pointMass, totalMass,  visited );

    return comJacobian;
}




