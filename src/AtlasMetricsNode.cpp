/*
 * AtlasMetricsNode.cpp
 *
 *  Created on: Apr 10, 2013
 *      Author: andrew.somerville
 */


#include <re2uta/debug.hpp>
#include <re2uta/AtlasLookup.hpp>
#include <re2/visutils/VisualDebugPublisher.h>
#include <re2uta/calc_point_mass.hpp>
#include <re2/eigen/eigen_util.h>
#include <re2/matrix_conversions.h>

#include <robot_state_publisher/robot_state_publisher.h>
#include <tf/transform_broadcaster.h>
#include <tf/transform_listener.h>
#include <sensor_msgs/JointState.h>
#include <ros/ros.h>

#include <kdl_parser/kdl_parser.hpp>
#include <urdf_model/model.h>

#include <Eigen/Core>

#include <boost/foreach.hpp>

#include <iostream>
#include <fstream>


using namespace re2uta;

class AtlasMetricsNode
{
    private:
        ros::NodeHandle  m_node;
        KDL::Tree        m_tree;
        Eigen::VectorXd  m_treeJointPositions;
        Eigen::VectorXd  m_jointMins;
        Eigen::VectorXd  m_jointMaxs;
        double           m_publishPeriod;
        Eigen::Vector3d  m_lastComPosInWorld;
        Eigen::Vector3d  m_lastComVelInWorld;

        AtlasLookup::Ptr m_atlasLookup;

        ros::Timer            m_publishTimer;
        ros::Subscriber       m_jointStateSubscriber;
        tf::TransformListener m_tfListener;

        ros::Time             m_lastSampleTime;
        ros::Time             m_sampleTime;


        re2::VisualDebugPublisher::Ptr m_vizDebug;

    public:
        AtlasMetricsNode()
        {
            m_vizDebug.reset( new re2::VisualDebugPublisher( "atlas_metric_markers" ) );
            std::string urdfString;
            m_publishPeriod = 0.05;

            m_node.getParam( "/robot_description", urdfString );
            m_node.getParam( "/publish_period",    m_publishPeriod );

            urdf::Model urdfModel;
            urdfModel.initString( urdfString );
            kdlRootInertiaWorkaround( urdfString, m_tree );

            m_atlasLookup.reset( new AtlasLookup( urdfModel ) );
            m_treeJointPositions   = Eigen::VectorXd::Zero(m_tree.getNrOfJoints());

            m_jointStateSubscriber = m_node.subscribe( "/atlas/joint_states", 1,         &AtlasMetricsNode::handleJointStates, this );
            m_publishTimer         = m_node.createTimer( ros::Duration(m_publishPeriod), &AtlasMetricsNode::handleTimeout,     this );

            ROS_INFO_STREAM( "Publish period is: " << m_publishPeriod );
        }


        void handleJointStates( const sensor_msgs::JointStateConstPtr & jointStatesMsg )
        {
            m_sampleTime = ros::Time::now();
            int msgJointIndex = 0;
            BOOST_FOREACH( const std::string & jointName, jointStatesMsg->name )
            {
                int qnr = m_atlasLookup->jointNameToQnr( jointName );
                if( qnr >= 0 && qnr < m_treeJointPositions.rows() )
                    m_treeJointPositions( qnr ) = jointStatesMsg->position[msgJointIndex];

                ++ msgJointIndex;
            }
        }


        void handleTimeout( const ros::TimerEvent & event )
        {
            ROS_INFO_STREAM( "Publish!" );
            publishMassVector();
        }


        void publishMassVector()
        {
            Eigen::Vector4d pointMassInBase;
            pointMassInBase = calcPointMass( m_tree, m_tree.getSegment( "l_foot" )->second, m_treeJointPositions );


            Eigen::Affine3d     lFootToWorld = lookupTransform( "l_foot", "odom" );

            Eigen::Vector3d     groundNormalInWorld( 0, 0, 1 );
            Eigen::Vector3d     comInWorld = lFootToWorld * Eigen::Vector3d( pointMassInBase.head(3) );// fixme, wrong, but just for testing

            Eigen::Hyperplane3d groundPlaneInWorld( groundNormalInWorld, Eigen::Zero3d );
            Eigen::Vector3d     comProjectionInWorld = groundPlaneInWorld.projection( comInWorld );

            ros::Duration       sampleTimeDiff      = m_sampleTime   - m_lastSampleTime;
            Eigen::Vector3d     comVelInWorld       = (comInWorld    - m_lastComPosInWorld)/sampleTimeDiff.toSec();
            Eigen::Vector3d     comAccelInWorld     = (comVelInWorld - m_lastComVelInWorld)/sampleTimeDiff.toSec();

            Eigen::Vector3d     gravityAccelInWorld = Eigen::ZAxis3d * 9.81;
            Eigen::Vector3d     zmpAccelInWorld     = comAccelInWorld + gravityAccelInWorld;

            Eigen::ParametrizedLine3d zmpLineInWorld( comInWorld, zmpAccelInWorld.normalized() );
            Eigen::Vector3d     zmpInWorld = re2::intersectLineWithPlane( zmpLineInWorld, groundPlaneInWorld );

            Eigen::Array4d      fade( 1,1,1,0.5 );

            ROS_INFO_STREAM( "Pointmass " << pointMassInBase.transpose() << " comAccel norm " << comAccelInWorld.norm() << " vec " << comAccelInWorld.transpose() );


            m_vizDebug->publishSegment(    comInWorld, comProjectionInWorld, Red,          BoostStringHash()("centerofmassp"), "odom" );
            m_vizDebug->publishVectorFrom( comInWorld, comVelInWorld,        Green,        BoostStringHash()("centerofmassv"), "odom" );
//            m_vizDebug->publishVectorFrom( comInWorld, comAccelInWorld,      Blue,         BoostStringHash()("centerofmassa"), "odom" );
            m_vizDebug->publishSegment(    comInWorld, zmpInWorld,           Blue * fade,  BoostStringHash()("centerofmassz"), "odom" );

            m_lastComVelInWorld = comVelInWorld;
            m_lastComPosInWorld = comInWorld;
            m_lastSampleTime    = m_sampleTime;
        }


        Eigen::Affine3d lookupTransform( const std::string & fromFrame, const std::string & toFrame )
        {
            tf::StampedTransform tfTransform;
            m_tfListener.waitForTransform( toFrame, fromFrame, ros::Time(0), ros::Duration(2) );
            m_tfListener.lookupTransform( toFrame, fromFrame, ros::Time(0), tfTransform );

            Eigen::Matrix4d homoTransform = re2::convertTfToEigen4x4( tfTransform );

            return Eigen::Affine3d( homoTransform );
        }



        void go()
        {
            ROS_INFO( "GO!" );
            ros::spin();
        }
};


int main( int argc, char ** argv )
{
    ros::init( argc, argv, "AtlasMetrics" );

    AtlasMetricsNode atlasMetricsNode;

    atlasMetricsNode.go();

    return 0;
}

