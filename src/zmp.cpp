/*
 * zmp.cpp
 *
 *  Created on: Jan 21, 2013
 *      Author: somervil
 */




#include <re2uta/zmp.hpp>



namespace re2uta
{


Eigen::ParametrizedLine3d calcZeroMomentLine( const Eigen::Vector3d & sensedForceN, const Eigen::Vector3d & sensedTorqueNM )
{
    Eigen::Vector3d           momentArmDir;
    Eigen::Vector3d           zeroMomentPoint0M;
    Eigen::Vector3d           zeroMomentPoint1M;
    double                    distanceM;
    Eigen::ParametrizedLine3d zeroMomentLine;
    Eigen::Hyperplane3d       footPlane;

    momentArmDir      = sensedForceN.cross( sensedTorqueNM ).normalized();
    distanceM         = sensedTorqueNM.norm()/sensedForceN.norm();
    zeroMomentPoint0M = momentArmDir * distanceM;

    zeroMomentLine    = Eigen::ParametrizedLine3d( zeroMomentPoint0M, sensedForceN.normalized() );

    return zeroMomentLine;
}


Eigen::Vector3d calcZMP( const Eigen::Vector3d & originM, const Eigen::Vector3d & sensorLocM, const Eigen::Vector3d & sensedForceN, const Eigen::Vector3d & sensedTorqueNM )
{
    Eigen::Vector3d           zeroMomentPointM;
    Eigen::Hyperplane3d       footPlane;

    Eigen::ParametrizedLine3d zeroMomentLine;

    zeroMomentLine   = calcZeroMomentLine( sensedForceN, sensedTorqueNM );
    footPlane        = Eigen::Hyperplane3d( Eigen::ZAxis3d, originM );
    zeroMomentPointM = re2::intersectLineWithPlane( zeroMomentLine, footPlane );

    return zeroMomentPointM - (sensorLocM - originM);
}


}
