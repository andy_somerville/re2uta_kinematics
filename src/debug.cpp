/*
 * debug.cpp
 *
 *  Created on: Mar 28, 2013
 *      Author: andrew.somerville
 */

#include <re2uta/debug.hpp>


boost::shared_ptr<re2::VisualDebugPublisher>    vizDebugPublisher;
boost::shared_ptr<re2uta::AtlasDebugPublisher>  atlasDebugPublisher;

Eigen::Vector4d colorFromHash( int hash )
{
    double red   = ((hash >> 16) & 0x0000FF) /255.0;
    double green = ((hash >>  8) & 0x0000FF) /255.0;
    double blue  = ((hash >>  0) & 0x0000FF) /255.0;

    return Eigen::Vector4d( red, green, blue, 1 );
}


bool debugSingleStep = 0;
